<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
<fmt:setBundle basename="ApplicationResources" />
<title> <fmt:message key="title.crudusr"/> </title>
</head>
<body>
        <h1><fmt:message key="label.crudusr"/></h1>
        <c:url var="url" scope="page" value="/nocturne/addeditusr">
        	<c:param name="name" value=""/>
                <c:param name="description" value=""/>
                <c:param name="duration" value=""/>
                <c:param name="insert" value="true"/>
        </c:url>
        <a href="${url}"><fmt:message key="label.crudusr.add"/></a>
        <br/><br/>
        <table class="borderAll">
            <tr>
                <th><fmt:message key="label.crudusr.name"/></th>
                <th><fmt:message key="label.crudusr.id"/></th>
                <!--<th><fmt:message key="label.crudusr.password"/></th>-->
                <th><fmt:message key="label.crudusr.edit"/>/<fmt:message key="label.crudusr.delete"/></th>
            </tr>
            <c:forEach var="crudusr" items="${users}" varStatus="status">
                <c:set var="roles" value=""/>
                <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${crudusr.name}</td>
                    <td class="nowrap">${crudusr.id}</td>
                    <td class="nowrap">
                        <c:forEach var="userRole" items="${crudusr.roles}" varStatus="roleStatus">
                            <c:set var="roles" value="${roles}${roleStatus.first?'':':' }${userRole.role}"/>
                        </c:forEach>
                        ${roles}
                    </td>
                    <td class="nowrap">
                        <c:url var="updurl" scope="page" value="/nocturne/addeditusr">
                            <c:param name="name" value="${crudusr.name}"/>
                            <c:param name="id" value="${crudusr.id}"/>
                            <c:param name="password" value="${crudusr.password}"/>
                            <c:param name="role" value="${roles}"/>
                            <c:param name="insert" value="false"/>
                        </c:url>
                        <a href="${updurl}"><fmt:message key="label.crudusr.edit"/></a>
                        &nbsp;&nbsp;&nbsp;
                        <c:url var="delurl" scope="page" value="/nocturne/deleteusr">
                            <c:param name="id" value="${crudusr.id}"/>
                        </c:url>
                        <a href="${delurl}" onclick="return confirm('Are you sure to delete this user?')"><fmt:message key="label.crudusr.delete"/></a>
                    </td>
                    <c:remove var="roles"/>
                </tr>
            </c:forEach>
        </table>
</body>
</html>