<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<fmt:setBundle basename="ApplicationResources" />

<title><fmt:message key="title.setuannualweeklysch" /></title>
</head>
<body>
    
    <h1><fmt:message key="caption.menu.setupannualweeklysch"/></h1>
    <c:if test="${not empty error}">
        <p style="color:red;">Error: ${error}</p>
    </c:if>
    <c:if test="${not empty message}">
        <p style="color:green;">${message}</p>
    </c:if>
    <form action="${pageContext.request.contextPath}/nocturne/createannualweeklysch" method=post>
        <table cellpadding=4 cellspacing=2 border=0>
            <tr>
                <td><fmt:message key="label.setupannualweeklysch.annualsch" /></td>
                <td>
                    <input type="text" name="annualschyear" size=4 maxlength=4>
                </td>
            </tr>
        </table>
        <input type="submit" value="Create">
    </form>

</body>
</html>