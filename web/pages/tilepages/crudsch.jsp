<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <fmt:setBundle basename="ApplicationResources" />
        <title> <fmt:message key="title.crudsch"/> </title>
    </head>
    <body>
        <h2>
            <c:if test="${!empty message}">
                <fmt:message key="${message}" /> <br/>
            </c:if>
	</h2>
        <p style="color:red;">
            <c:if test="${!empty errorList}">
                <c:forEach var="error" items="${errorList}">
                    <fmt:message key="${error}" /> <br/>
                </c:forEach>
            </c:if>
        </p>
        <h1><fmt:message key="label.crudsch"/></h1>
        <c:url var="url" scope="page" value="/nocturne/addeditsch">
            <c:param name="name" value=""/>
            <c:param name="description" value=""/>
            <c:param name="duration" value=""/>
            <c:param name="insert" value="true"/>
        </c:url>
        <input type="hidden" id="copySchUrl" name="inputName" value="${pageContext.request.contextPath}/nocturne/copysch">

        <a href="${url}"><fmt:message key="label.crudsch.add"/></a>
        <input type='button' onclick = 'copySubmit();' value='Copy Schedule'>
        <br/><br/>

        <c:if test="${not empty error}">
            <p style="color:red;">Error: ${error}</p>
        </c:if>

        <c:if test="${not empty success}">
            <p style="color:red;">Success: ${success}</p>
        </c:if>
        <form action="${pageContext.request.contextPath}/nocturne/searchsch" name="frm" method="post">
            <table cellpadding=4 cellspacing=2 border=0>
                <tr>
                    <th colspan="2"><fmt:message key="label.crudsch.search" /></th>
                </tr>
                <tr>
                    <td><fmt:message key="label.crudsch.startDate" /></td>
                    <td>
                        <input type="text" name="startDate" size=15 maxlength=10 id="datepicker">
                    </td>
                </tr>
            </table>
            <input type="submit" value="Search">

            <br/><br/>
            <table class="borderAll">
                <tr>
                    <th></th>
                    <th><fmt:message key="label.crudsch.programDate"/></th>
                    <th><fmt:message key="label.crudsch.programDuration"/></th>
                    <th><fmt:message key="label.crudsch.programName"/></th>
                    <th><fmt:message key="label.crudsch.presenter"/></th>
                    <th><fmt:message key="label.crudsch.producer"/></th>
                    <th><fmt:message key="label.crudsch.edit"/> <fmt:message key="label.crudsch.delete"/></th>
                </tr>
                <c:forEach var="sch" items="${rps}" varStatus="status">
                    <tr class="${status.index%2==0?'even':'odd'}">
                        <td class ="nowrap"><input type="checkbox" id="checkbox" name="name" value="${sch.dateOfProgram}"></td>
                        <td class="nowrap">${sch.dateOfProgram}</td>
                        <td class="nowrap">${sch.duration}</td>
                        <td class="nowrap">${sch.radioProgram.name}</td>
                        <td class="nowrap">${sch.presenter.name}</td>
                        <td class="nowrap">${sch.producer.name}</td>

                        <td class="nowrap">
                            <c:url var="updurl" scope="page" value="/nocturne/addeditsch">
                                <c:param name="programToBeDeleted" value="${sch.dateOfProgram}" />
                                <c:param name="insert" value="false"></c:param>
                            </c:url>
                            
                            <jsp:useBean id="today" class="java.util.Date" />
                           
                            
                            <c:choose> 
                                 <c:when test="${today.time gt sch.dateOfProgram.time}">
                                          <fmt:message key="label.crudsch.edit"/>
                                           &nbsp;&nbsp;&nbsp;
                                 </c:when>
                                 <c:otherwise>
                                          <a href="${updurl}"><fmt:message key="label.crudsch.edit"/></a>
                                           &nbsp;&nbsp;&nbsp;
                                 </c:otherwise>
                            </c:choose>
    
                           
                                   
                          <!--  <a href="${updurl}"><fmt:message key="label.crudsch.edit"/></a>
                            &nbsp;&nbsp;&nbsp; -->

                            <c:url var="delurl" scope="page" value="/pages/setupsch.jsp">
                                <c:param name="dateOfProgram" value="${sch.dateOfProgram}"/>
                                <c:param name="duration" value="${sch.duration}"/>
                                <c:param name="name" value="${sch.radioProgram.name}"/>
                                <c:param name="preName" value="${sch.presenter.name}"/>
                                <c:param name="proName" value="${sch.producer.name}"/>
                                <c:param name="delete" value="true"/>
                            </c:url>
                           <!-- <a href="${delurl}"><fmt:message key="label.crudsch.delete"/></a>-->
                            
                             <c:choose> 
                                 <c:when test="${today.time gt sch.dateOfProgram.time}">
                                          <fmt:message key="label.crudsch.delete"/>
                                           &nbsp;&nbsp;&nbsp;
                                 </c:when>
                                 <c:otherwise>
                                          <a href="${delurl}"><fmt:message key="label.crudsch.delete"/></a>
                                           &nbsp;&nbsp;&nbsp;
                                 </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <br>
        </form>
        <br/><br/>



        <script>
            $(function () {
                var daysToDisable = [1, 2, 3, 4, 5, 6];

                $("#datepicker").datepicker({
                    beforeShowDay: disableSpecificWeekDays,
                    dateFormat: 'dd/mm/yy'
                });

                function disableSpecificWeekDays(date) {
                    var day = date.getDay();
                    for (i = 0; i < daysToDisable.length; i++) {
                        if ($.inArray(day, daysToDisable) != -1) {
                            return [false];
                        }
                    }
                    return [true];
                }
            });

            function copySubmit() {
                document.frm.action = document.getElementById('copySchUrl').value;
                document.frm.submit();
            }

        </script>
    </body>
</html>