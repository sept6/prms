<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
<fmt:setBundle basename="ApplicationResources" />
<title> <fmt:message key="title.crudsch"/> </title>
</head>
<body>
   
    <h1><fmt:message key="label.crudsch"/></h1>
    <c:url var="url" scope="page" value="/nocturne/addeditrp">
            <c:param name="name" value=""/>
            <c:param name="description" value=""/>
            <c:param name="duration" value=""/>
            <c:param name="insert" value="true"/>
    </c:url>
   
    <br/><br/>

    <c:if test="${not empty error}">
        <p style="color:red;">Error: ${error}</p>
    </c:if>
        
    <form action="${pageContext.request.contextPath}/nocturne/confirmcopysch" name="frm" method="post">
        <table cellpadding=4 cellspacing=2 border=0>
            <tr>
                <th colspan="2"><fmt:message key="label.crudsch.copy" /></th>
            </tr>
            <!--
            <tr>
                <td><fmt:message key="label.crudsch.startDate" /></td>
                <td>
                    <input type="text" name="startDate" size=15 maxlength=10 id="datepicker">
                </td>
            </tr>
            <tr>
                <td><fmt:message key="label.copysch.duration" /></td>
                <td>
                    <input type="text" name="duration" size=15 maxlength=10 id="duration">
                </td>
            </tr>-->
        </table>
        <input type="submit" value="Confirm Copy">
        
        <br/><br/>
        <table class="borderAll">
        <tr>
            
            <th><fmt:message key="label.crudsch.programDate"/></th>
             <th><fmt:message key="label.setupsch.startTime"/></th>
            <th><fmt:message key="label.crudsch.programDuration"/></th>
            <th><fmt:message key="label.crudsch.programName"/></th>
            <th><fmt:message key="label.crudsch.presenter"/></th>
            <th><fmt:message key="label.crudsch.producer"/></th>
        </tr>
        <c:forEach var="sch" items="${copyrps}" varStatus="status">
            <tr class="${status.index%2==0?'even':'odd'}">
                <td class="nowrap"> <input type="text" name="dateOfProgram${status.index}" value="" 
                                           size=8 id="datepicker${status.index}" readonly="readonly" class="datepicker"></td>
                <td>
                 <select name="starttime${status.index}" id="starttime${status.index}" class="starttime"></select><fmt:message key="label.setupsch.hours" />&nbsp;
                  <select name="starttimemm${status.index}" id="starttimemm${status.index}">
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select><fmt:message key="label.setupsch.minutes" />
                </td>
                <td class="nowrap">${sch.duration}</td>
                <td class="nowrap">${sch.radioProgram.name}</td>
                <td class="nowrap">${sch.presenter.name}</td>
                <td class="nowrap">${sch.producer.name}</td>
            </tr>
        </c:forEach>
         </table>
    </form>
    <br/><br/>

    
        
    <script>
        $( function() {
            for(var i=0; i<24;i++){
                var formatHH = ("0" + i).slice(-2);
               $(".starttime").append('<option value='+formatHH+'>'+formatHH+'</option>');
            }
            var dateToday = new Date(); 
        $( ".datepicker" ).datepicker({
              dateFormat: 'dd/mm/yy',
              minDate: dateToday
          });
    } );
        
     
    </script>
</body>
</html>