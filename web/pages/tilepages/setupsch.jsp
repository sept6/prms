<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<fmt:setBundle basename="ApplicationResources" />

<title><fmt:message key="title.setupsch" /></title>
<script>
    $( function() {
       
        for(var i=0; i<24;i++){
            var formatHH = ("0" + i).slice(-2);
           $("#starttimehh").append('<option value='+formatHH+'>'+formatHH+'</option>');
        }
        
        $("#selectProgramName" ).change(function(){
            var selectedProgram = $('#selectProgramName').val();
            var duration = selectedProgram.substring(selectedProgram.lastIndexOf("||")+2,selectedProgram.length-1); 
            document.getElementById('duration').value = duration;
            //alert(document.getElementById('duration').value);

        });
         var dateToday = new Date(); 
        $( "#dateOfProgram" ).datepicker({
              dateFormat: 'dd/mm/yy',
               minDate: dateToday
          });
        $( "#createschform" ).submit(function( event ) {
            document.getElementById('starttime').value = $('#starttimehh').val() + ':' + $('#starttimemm').val();
            var selectedProgram = $('#selectProgramName').val();
            document.getElementById('programName').value = selectedProgram.substring(0,selectedProgram.lastIndexOf("||"));
        });
     
    } );

</script>
</head>
<body>
    
    <c:if test="${not empty errorList}">
        <p style="color:red;">
        <fmt:message key="${message}" /> <br/>
        <c:if test="${!empty errorList}">
           <c:forEach var="error" items="${errorList}">
               <fmt:message key="${error}" /> <br/>
           </c:forEach>
        </c:if>
        </p>
    </c:if>
        
    <c:if test="${param['delete'] eq null}">
	<form action="${pageContext.request.contextPath}/nocturne/createsch" method=post id="createschform">
		<center>
			<table cellpadding=4 cellspacing=2 border=0>
				<tr>
					<td><fmt:message key="label.setupsch.programName" /></td>
					<td><c:if test="${param['insert'] == 'true'}">
                                                <select name="selectProgramName" id="selectProgramName">
                                                    <c:forEach var="program" items="${programList}">
                                                        <option value="${program.name}||${program.typicalDuration}">${program.name} </option>
                                                    </c:forEach>
                                                </select>           
						<input type="hidden" name="programName" id="programName" value="${param['programName']}" size=20 maxlength=20/>
						<input type="hidden" name="insert" value="true" />
                                                &nbsp;&nbsp;<fmt:message key="label.setupsch.duration" /> 
                                                <input type="text" name="duration" value="" size=8 maxlength=8 id="duration"/>
                                            </c:if> 
                                            <c:if test="${param['insert']=='false'}">
                                                <select name="selectProgramName" id="selectProgramName">
                                                    <c:forEach var="program" items="${programList}">
                                                        <option value="${program.name}||${program.typicalDuration}" <c:if test="${program.name eq param['programName']}">selected</c:if> > ${program.name} </option>
                                                    </c:forEach>
                                                </select>
                                                <input type="hidden" name="programToBeDeleted" id="programToBeDeleted" value="${param['programToBeDeleted']}" />
						<input type="hidden" name="programName" id="programName" value="${param['programName']}" size=20 maxlength=20/>
						<input type="hidden" name="insert" value="false" />
                                                &nbsp;&nbsp;<fmt:message key="label.setupsch.duration" /> 
                                                <input type="text" name="duration" value="${param['duration']}" size=8 maxlength=8 id="duration"/>                                                
					    </c:if>
                                        </td>
				</tr>
				<tr>
					<td><fmt:message key="label.setupsch.presenter" /></td>
                                        <td>
                                            <select name="presenterId" id="presenterId">
                                                <c:forEach var="preparePresenter" items="${presenters}">
                                                    <option value="${preparePresenter.id}" <c:if test="${preparePresenter.id eq param['presenterId']}">selected</c:if>>${preparePresenter.name}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
				</tr>
				<tr>
					<td><fmt:message key="label.setupsch.producer" /></td>
                                        <td>
                                            <select name="producerId" id="producerId">
                                                <c:forEach var="prepareProducer" items="${producers}">
                                                    <option value="${prepareProducer.id}" <c:if test="${prepareProducer.id eq param['producerId']}">selected</c:if>>${prepareProducer.name}</option>
                                                </c:forEach>
                                            </select>  
                                        </td>
				</tr>
				<tr>
					<td><fmt:message key="label.setupsch.dateOfProgram" /></td>
					<td><input type="text" name="dateOfProgram" value="${param['dateOfProgram']}" size=8 id="dateOfProgram" readonly="readonly">
                                        &nbsp; 
                                        <fmt:message key="label.setupsch.startTime" /> 
                                        <select name="starttimehh" id="starttimehh"></select><fmt:message key="label.setupsch.hours" />&nbsp;
                                        <select name="starttimemm" id="starttimemm">
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select>
                                        <input type="hidden" name="starttime" value="${param['starttime']}" size=8 id="starttime"/> &nbsp;<fmt:message key="label.setupsch.minutes" />

                                        </td>
				</tr>
			</table>
		</center>
		<input type="submit" value="Submit"/> 
                <input type="reset" value="Reset"/>
	</form>
    </c:if>
    <c:if test="${param['delete'] == 'true'}">
        <form action="${pageContext.request.contextPath}/nocturne/deletesch" method=post id="deleteschform">
            <input type="hidden" name="dateOfProgram" id="dateOfProgram" value="${param['dateOfProgram']}" size=20 maxlength=20/>
            <center>
                <table cellpadding=4 cellspacing=2 border=0>
                    <tr>
                        <td><fmt:message key="label.setupsch.dateOfProgram" /></td>
                        <td>         
                            : <%= request.getParameter("dateOfProgram") %>
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.setupsch.duration" /></td>
                        <td>         
                            : <%= request.getParameter("duration") %>
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.setupsch.programName" /></td>
                        <td>         
                            : <%= request.getParameter("name") %>
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.setupsch.presenter" /></td>
                        <td>         
                            : <%= request.getParameter("preName") %>
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.setupsch.producer" /></td>
                        <td>         
                            : <%= request.getParameter("proName") %>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Delete Confirm"/>
            </center>
        </form>
    </c:if>
</body>
</html>