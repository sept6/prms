<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<fmt:setBundle basename="ApplicationResources" />
<c:set var="t" value="true" />
<title><fmt:message key="title.error" /></title>
</head>
<body>
        <c:if test="${!empty message}">
            <h2>
                <fmt:message key="${message}" /> <br/>
            </h2>
        </c:if>
        <p style="color:red;">
            <c:if test="${!empty errorList}">
                <c:forEach var="error" items="${errorList}">
                    <fmt:message key="${error}" /> <br/>
                </c:forEach>
            </c:if>
        </p>
</body>
