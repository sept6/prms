<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<fmt:setBundle basename="ApplicationResources" />
<script>
    $( function() {

        $( "#createmodifyuser" ).submit(function( event ) {
            var multiSelectSize = document.getElementById('selectrole').options.length;
            var role ="";
            //alert(multiSelectSize);
            
            for (var i=0; i <multiSelectSize; i++){
                if(document.getElementById('selectrole').options[i].selected){
                    if(role!==""){
                        role= role+":";
                    }
                    role = role + document.getElementById('selectrole').options[i].value;
                }
            } 
            document.getElementById('role').value = role;
        });
    } );

</script>
<title><fmt:message key="title.setupusr" /></title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/nocturne/${(param['insert']==false)?'modifyuser':'createuser'}" method=post id='createmodifyuser'>
		<center>
			<table cellpadding=4 cellspacing=2 border=0>
<!--				<tr>
					<td><fmt:message key="label.crudusr.name" /></td>
					<td><c:if test="${param['insert'] == 'true'}">
							<input type="text" name="name" value="${param['name']}" size=15
								maxlength=20>
							<input type="hidden" name="ins" value="true" />
						</c:if> 
						<c:if test="${param['insert']=='false'}">
							<input type="text" name="name" value="${param['name']}" size=15
								maxlength=20 readonly="readonly">
							<input type="hidden" name="ins" value="false" />
						</c:if></td>
				</tr>
				<tr>
					<td><fmt:message key="label.crudusr.id" /></td>
					<td><input type="text" name="id"
						value="${param['id']}" size=15 maxlength=15></td>
				</tr>-->
                                                
				<tr>
                                    <td><fmt:message key="label.crudusr.id" /></td>
                                    <td>
                                        <c:if test="${param['insert'] == 'true'}">
                                            <input type="text" name="id" value="${param['id']}" size=15 maxlength=15>
                                        </c:if>
                                        <c:if test="${param['insert']=='false'}">
                                            <input type="text" name="id" value="${param['id']}" readonly="readonly">
                                            <input type="hidden" name="hid" value="${param['id']}" />
                                        </c:if>
                                    </td>
				</tr>
                                <tr>
                                    <td><fmt:message key="label.crudusr.name" /></td>
                                    <td>                                        
                                        <input type="text" name="name" value="${param['name']}" size=15
                                                maxlength=20>
                                        <input type="hidden" name="ins" value="true" />
                                    </td>
				</tr>
				<tr>
					<td><fmt:message key="label.crudusr.password" /></td>
					<td><input type="password" name="password"
						value="${param['password']}" size=15 maxlength=20></td>
				</tr>
				<tr>
					<td><fmt:message key="label.crudusr.role" /></td>
					<td><input type="hidden" name="role" id="role"
						value="${param['role']}">
                                        <select name="selectrole" id="selectrole" multiple>
                                          <option value="manager" <c:if test="${fn:contains(param['role'], 'manager')}">selected</c:if>>Manager</option>
                                          <option value="admin" <c:if test="${fn:contains(param['role'], 'admin')}">selected</c:if>>System Administrator</option>
                                          <option value="presenter" <c:if test="${fn:contains(param['role'], 'presenter')}">selected</c:if>>Presenter</option>
                                          <option value="producer" <c:if test="${fn:contains(param['role'], 'producer')}">selected</c:if>>Producer</option>
                                        </select>
                                        <br/> *<i>Hold down the Ctrl(windows)/Command(Mac) button to select multiple options.</i>
                                        </td>
				</tr>                                
			</table>
		</center>
		<input type="submit" value="Submit"> <input type="reset"
			value="Reset">
	</form>

</body>
</html>