/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.radioprogram.dao.ProgramDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;

/**
 *
 * @author andy
 * @author Zaw Min Thant, added processModify method
 */
public class ScheduleService {

    private DAOFactoryImpl factory;
    private ScheduleDAO schdao;
    private ProgramDAO rpdao;
    private UserDao udao;

    public ScheduleService() {
        super();
        factory = new DAOFactoryImpl();
        schdao = factory.getScheduleDAO();
        rpdao = factory.getProgramDAO();
        udao = factory.getUserDAO();
    }

    public void setScheduleDAO(ScheduleDAO dao) {
        schdao = dao;
    }

    /**
     * Copy a program slot.
     *
     * @param programSlot - A list of program slot
     */
    public void processCopy(List<ProgramSlot> programSlot) {
        for (ProgramSlot slot : programSlot) {
            try {
                schdao.create(slot);
            } catch (NotFoundException | SQLException ex) {
                Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Create annual weekly schedule.
     *
     * @param annualSch - An annual schedule.
     * @return error - Return an error message if any error encountered.
     * Otherwise, return empty string.
     */
    public String processCreateAnnualWeeklySchedule(AnnualSchedule annualSch) {
        // Validate cannot create past schedule
        if (annualSch.getYear() < Calendar.getInstance().get(Calendar.YEAR)) {
            return "Cannot create past annual schedule";
        }

        // Validate duplicate schedule
        try {
            if (schdao.isAnnualScheduleExist(annualSch.getYear())) {
                return "Annual Schedule already exist";
            }

            annualSch.setWeeklySchedules(generateWeeklySchedule(annualSch));

            schdao.createAnnualWeeklySchedule(annualSch);

        } catch (SQLException ex) {
            Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }

        return "";
    }

    /**
     * Create weekly schedule date for every Sunday of the annual schedule year
     *
     * @param annualSch annual schedule object
     * @return array list of weekly schedule
     */
    public ArrayList<WeeklySchedule> generateWeeklySchedule(AnnualSchedule annualSch) {
        ArrayList<WeeklySchedule> weeklySchedules = new ArrayList<>();
        Calendar cal = new GregorianCalendar(annualSch.getYear(), 0, 1);
        do {
            int day = cal.get(Calendar.DAY_OF_WEEK);
            if (day == Calendar.SUNDAY) {
                WeeklySchedule weeklySch = new WeeklySchedule();
                weeklySch.setStartDate(cal.getTime());
                weeklySch.setAssignedBy(annualSch.getAssignedBy());
                weeklySchedules.add(weeklySch);
            }
            cal.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal.get(Calendar.YEAR) == annualSch.getYear());
        return weeklySchedules;
    }

    /**
     * Create a program slot.
     *
     * @param programSlot - A program slot
     */
    public void processCreate(ProgramSlot programSlot) {
        try {
            schdao.create(programSlot);
        } catch (NotFoundException | SQLException ex) {
            Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Modify the selected schedule of program.
     *
     * @param newProgramSlot - New program slot to be saved.
     * @param oldProgramSlot - Old program slot to be deleted.
     */
    public void processModify(ProgramSlot newProgramSlot, ProgramSlot oldProgramSlot) {
        try {
           /* if (schdao.create(newProgramSlot)) {
                processDelete(oldProgramSlot);
            }*/
           //delete the schedule first if the user select the original program date time
           processDelete(oldProgramSlot);
           schdao.create(newProgramSlot);
        } catch (NotFoundException | SQLException ex) {
            Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Delete the selected program slot.
     *
     * @param programSlot - Program slot to be deleted.
     * @return error - Error message.
     */
    public String processDelete(ProgramSlot programSlot) {
        try {
            if (programSlot.getStartTime().before(new Date())) {
                // Presented or Presenting program are not allow to delete.
                return "error.deletePresentingPresentedError";
            }
            schdao.delete(programSlot);
        } catch (NotFoundException | SQLException ex) {
            Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Check whether the time slot is available or not.
     *
     * @param dateProgramTime - time of the program slot
     * @return result - Return true if the candidate time slot is available.
     * Otherwise, return false.
     */
    public boolean isTimeSlotAvailable(Timestamp dateProgramTime) {
        boolean result = false;
        try {
            result = schdao.isTimeSlotAvailable(dateProgramTime);
        } catch (SQLException ex) {
            Logger.getLogger(ScheduleService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
