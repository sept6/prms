/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andy
 */
@Action("managesch")
public class ManageScheduleCmd implements Perform{

    /**
	 * perform. This will redirect to maintain schedule page
	 * 
	 *
     * @return maintain schedule page
     * @throws java.io.IOException throw IO exception
     * @throws javax.servlet.ServletException throw server exception error
     * 
	 */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
       return "/pages/crudsch.jsp";
    }
    
}
