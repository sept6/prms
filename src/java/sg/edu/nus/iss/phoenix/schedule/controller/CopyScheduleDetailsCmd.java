/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.util.Utility;

@Action("confirmcopysch")
public class CopyScheduleDetailsCmd implements Perform{

     /**
     * perform. This will save the program slot into database
     * @param path Redirect path
     * @param req  HttpRequest
     * @param resp HttpResponse
     * @return copy schedule page
     * @throws java.io.IOException throw IO exception
     * @throws javax.servlet.ServletException throw servlet exception
     * @author andy
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ScheduleDelegate svc = new ScheduleDelegate();
        if(req.getSession().getAttribute("selectedProgramSlot") == null){
            req.setAttribute("error", "Please select at least one program slot to copy.");
            return "/pages/crudsch.jsp";
        }
        
        List<ProgramSlot> selectedProgramSlot = (List<ProgramSlot>) req.getSession().getAttribute("selectedProgramSlot");
        
        for(int i = 0; i < selectedProgramSlot.size(); i++){
            ProgramSlot slot = selectedProgramSlot.get(i);
            String dateProgram = req.getParameter("dateOfProgram" + i);
            String startTime = req.getParameter("starttime" + i);
            String startMinute = req.getParameter("starttimemm" + i);
            String programStartDateTime = dateProgram + " " + startTime + ":" + startMinute + ":00";
            Date programStartDate = Utility.toDate(programStartDateTime,Utility.DATE_TIME_PATTERN);
            slot.setDateOfProgram(programStartDate);
            slot.setStartTime(programStartDate);
            
            //check if timeslot available
            boolean result = svc.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()));
            if(!result){
                req.setAttribute("error", programStartDate + " is not available. Please choose another time slot");
                req.setAttribute("copyrps", selectedProgramSlot);
                return "/pages/copysch.jsp";
            }
        }
        
        svc.processCopy(selectedProgramSlot);
        req.setAttribute("success", "List of Program copied successfully.");
        req.getSession().removeAttribute("selectedProgramSlot");
        return "/pages/crudsch.jsp";
    }
    
}
