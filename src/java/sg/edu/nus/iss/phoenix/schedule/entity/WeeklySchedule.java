package sg.edu.nus.iss.phoenix.schedule.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * WeeklySchedule Value Object. This class is value object representing database table
 * weekly-schedule This class is intended to be used together with associated Dao object.
 * @author Kevin
 */
public class WeeklySchedule implements Cloneable, Serializable {
    
    private Date startDate;
    private String assignedBy;
    private ArrayList<ProgramSlot> programSlots = new ArrayList<ProgramSlot>();
    
    /**
     * This constructor creates an instance of WeeklySchedule object
     */
    public WeeklySchedule() {
        
    }
    
    /**
     * This constructor creates an instance of WeeklySchedule object
     * @param startDate weekly schedule start date
     */
    public WeeklySchedule(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * This method returns the weekly schedule start date
     * @return the weekly schedule start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * This method set the weekly schedule start date
     * @param startDate the weekly schedule start date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * This method return the weekly schedule creator
     * @return the weekly schedule creator
     */
    public String getAssignedBy() {
        return assignedBy;
    }

    /**
     * This method set the weekly schedule creator
     * @param assignedBy the weekly schedule creator
     */
    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    /**
     * This method returns the list of program slot inside the weekly schedule
     * @return the list of program slot inside the weekly schedule
     */
    public ArrayList<ProgramSlot> getProgramSlots() {
        return programSlots;
    }

    /**
     * This method set the list of program slot for the weekly schedule
     * @param programSlots the list of program slot for the weekly schedule
     */
    public void setProgramSlots(ArrayList<ProgramSlot> programSlots) {
        this.programSlots = programSlots;
    }
}
