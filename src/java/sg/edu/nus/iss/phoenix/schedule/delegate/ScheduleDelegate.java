/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.delegate;

import java.sql.Timestamp;
import java.util.List;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.service.ScheduleService;

/**
 *
 * @author andy
 * @author Raghavendra S, added processCreate method
 * @author Zaw Min Thant, added processModify method
 */
public class ScheduleDelegate {
    
    public void processCopy(List<ProgramSlot> programSlot){
        ScheduleService svcSch = new ScheduleService();
        svcSch.processCopy(programSlot);
    }
    
    public String processCreateAnnualWeeklySchedule(AnnualSchedule annualSch) {
        ScheduleService svcSch = new ScheduleService();
        return svcSch.processCreateAnnualWeeklySchedule(annualSch);
    }
    
    public void processCreate(ProgramSlot programSlot){
        ScheduleService svcSch = new ScheduleService();
        svcSch.processCreate(programSlot);
    }
    
    public void processModify(ProgramSlot programSlot, ProgramSlot programToBeDeleted) {
        ScheduleService scheduleService = new ScheduleService();
        scheduleService.processModify(programSlot, programToBeDeleted);
    }
	
    public boolean isTimeSlotAvailable(Timestamp dateProgramTime){
        ScheduleService svcSch = new ScheduleService();
        return svcSch.isTimeSlotAvailable(dateProgramTime);
    }
    
    public String processDelete(ProgramSlot programSlot){
        ScheduleService svcSch = new ScheduleService();
        return svcSch.processDelete(programSlot);
    }
}
