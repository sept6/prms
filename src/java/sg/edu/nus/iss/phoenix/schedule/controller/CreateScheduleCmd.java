/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 * This class is a controller for create program schedule request, which
 * receives the create requests, perform basic validation and invokes the
 * <code>processCreate()</code> method of the <code>ScheduleDelegate</code> to
 * create a Program Slot with the provided Program Slot attributes.
 *
 * @author Raghavendra S
 * @author Zaw Min Thant, added implementation for Modify Schedule feature
 */
@Action("createsch")
public class CreateScheduleCmd implements Perform {

    /**
     * This method perform basic validation of the parameters submitted as part
     * of the request and invokes the <code>processCreate()</code> method of the
     * <code>ScheduleDelegate</code> to create a Program Slot with the provided
     * Program Slot attributes.
     * 
     * @param path forward url path
     * @param req HttpServlet request
     * @param resp HttpServlet response
     * @throws javax.servlet.ServletException throw javax.servlet exception
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        List<String> errorList = new ArrayList();
        String programName = req.getParameter("programName");
        String presenterId = req.getParameter("presenterId");
        String producerId = req.getParameter("producerId");
        String strDateOfProgram = req.getParameter("dateOfProgram");
        String strStartTime = req.getParameter("starttime");
        String successPath = "/pages/success.jsp";
        String errorPath = req.getParameter("errorPath");
        errorPath = (req.getParameter("errorPath")!=null && !errorPath.equals(""))?path:"/pages/customerror.jsp";;

        String dateProgramTime = strDateOfProgram + " " + strStartTime + ":00";
        Date programStartDate = Utility.toDate(dateProgramTime, Utility.DATE_TIME_PATTERN);

        String duration = req.getParameter("duration");
        String assignedBy = ((User) req.getSession().getAttribute("user")).getId();
        ScheduleDelegate scheduleDelegate = new ScheduleDelegate();

        if (Utility.isEmpty(programName)) {
            errorList.add("error.invalidprogramname");
        }
        if (Utility.isEmpty(presenterId)) {
            errorList.add("error.invalidpresenterid");
        }
        if (Utility.isEmpty(producerId)) {
            errorList.add("error.invalidproducerid");
        }
        if (Utility.isEmpty(strDateOfProgram) || Utility.isEmpty(strStartTime)) {
            errorList.add("error.invalidstartdatetime");
        }
        
        /*
        if (!scheduleDelegate.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()))) {
            errorList.add("error.timeslotnotavailable");
        }*/

        if (Utility.isEmpty(duration)) {
            errorList.add("error.invalidduration");
        } else if (!duration.matches("[\\d]{2}:[\\d]{2}:[\\d]+")) {
            errorList.add("error.invalidduration");
        }

        
        if (errorList.size() > 0) {
            req.setAttribute("message", "error.createschedulefailed");
            req.setAttribute("errorList", errorList);
            req.setAttribute("insert", req.getParameter("insert"));
            return errorPath;
        }
        
        ProgramSlot programSlot = new ProgramSlot();

        RadioProgram radioProgram = new RadioProgram();
        radioProgram.setName(programName);

        User presenter = new User();
        presenter.setId(presenterId);

        User producer = new User();
        producer.setId(producerId);

        try {

            programSlot.setStartTime(programStartDate);
            programSlot.setDateOfProgram(programStartDate);
            programSlot.setRadioProgram(radioProgram);
            programSlot.setPresenter(presenter);
            programSlot.setProducer(producer);
            programSlot.setAssignedBy(assignedBy);
            programSlot.setDuration(Time.valueOf(duration));

            String insert = (String) req.getParameter("insert");

            if (insert.equalsIgnoreCase("true")) {
                  if (!scheduleDelegate.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()))) {
                    errorList.add("error.timeslotnotavailable");
                  }
                  
                  if (errorList.size() > 0) {
                    req.setAttribute("message", "error.createschedulefailed");
                    req.setAttribute("errorList", errorList);
                    req.setAttribute("insert", req.getParameter("insert"));
                    return errorPath;
                }
                  
                scheduleDelegate.processCreate(programSlot);
                req.setAttribute("message", "message.createschedulesucess");
            } else {
                ProgramSlot oldProgramSlot = new ProgramSlot();

                String strDateOfOldProgram = req.getParameter("programToBeDeleted");

              //  String[] strDateOfOldProgramArr = strDateOfOldProgram.split(" ");
              //  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat dateFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    
               // Date dateOfProgram;
                Date timeOfProgram;
                
                try {
                   // dateOfProgram = dateFormat.parse(strDateOfOldProgramArr[0]);
                    timeOfProgram = dateFormatTime.parse(strDateOfOldProgram);
                    oldProgramSlot.setStartTime(timeOfProgram);
                    oldProgramSlot.setDateOfProgram(timeOfProgram);
                } catch (ParseException ex) {
                    Logger.getLogger(CreateScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
                    req.setAttribute("message", "error.deleteschedulefailed");
                    return errorPath;
                }

                if(programSlot.getDateOfProgram().compareTo(oldProgramSlot.getDateOfProgram()) != 0){
                    if (!scheduleDelegate.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()))) {
                        errorList.add("error.timeslotnotavailable");
                    }

                    if (errorList.size() > 0) {
                        req.setAttribute("message", "error.createschedulefailed");
                        req.setAttribute("errorList", errorList);
                        req.setAttribute("insert", req.getParameter("insert"));
                        return errorPath;
                    }
                }
                scheduleDelegate.processModify(programSlot, oldProgramSlot);
                req.setAttribute("message", "message.updateschedulesucess");
            }

        } catch (Exception ex) {
            Logger.getLogger(CreateScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
            req.setAttribute("message", "error.createschedulefailed");
            return errorPath;
        }

        return successPath;
    }
}
