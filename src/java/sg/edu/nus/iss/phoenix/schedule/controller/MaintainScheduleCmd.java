/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.radioprogram.delegate.ReviewSelectProgramDelegate;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.radioprogram.service.ReviewSelectProgramService;
import sg.edu.nus.iss.phoenix.schedule.delegate.ReviewSelectPresenterProducerDelegate;

/**
 *
 * @author Raghavendra S
 */
@Action("addeditsch")
public class MaintainScheduleCmd implements Perform {
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ReviewSelectPresenterProducerDelegate rsppDelegate = new ReviewSelectPresenterProducerDelegate();
        List<User> presenterList = null;
        List<User> producerList = null;
        List<RadioProgram> radioProgramList = null;
        ReviewSelectProgramDelegate programService = new ReviewSelectProgramDelegate();
        presenterList = rsppDelegate.reviewPresenter();
        producerList = rsppDelegate.reviewproducer();
        
        radioProgramList = programService.reviewSelectRadioProgram();
        String name = presenterList.get(0).getId();
        
        String programToBeDeleted = req.getParameter("programToBeDeleted");
        req.setAttribute("programToBeDeleted", programToBeDeleted);
        
        req.setAttribute("presenters", presenterList);
        req.setAttribute("producers", producerList);
        req.setAttribute("programList", radioProgramList);
        
        return "/pages/setupsch.jsp";
    } 
}
