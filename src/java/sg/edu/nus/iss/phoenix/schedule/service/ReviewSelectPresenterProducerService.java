/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.RoleDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.radioprogram.dao.ProgramDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;

/**
 *
 * @author user
 */
public class ReviewSelectPresenterProducerService {
    private DAOFactoryImpl factory;
    private ScheduleDAO schdao;
    private ProgramDAO rpdao;
    private UserDao udao;
    private RoleDao rdao;
    
    public ReviewSelectPresenterProducerService() {
        super();
        factory = new DAOFactoryImpl();
        schdao = factory.getScheduleDAO();
        rpdao = factory.getProgramDAO();
        udao = factory.getUserDAO();
        rdao = factory.getRoleDAO();
    }
    
    public List<User> displayPresenter(){
        List<User> presenterlist = new ArrayList();
        List<User> tempList = new ArrayList();
        Boolean presenterRoleFlag = false;
        try{
            tempList = udao.loadAll();
            int userListLength = tempList.size();
            for(int i = 0; i < userListLength; i++){
                User tempUser = tempList.get(i);
                List tempRole = tempUser.getRoles();
                for(int j = 0; j < tempRole.size(); j++){
                    String role = tempUser.getRoles().get(j).getRole();
                    if(role.equals("presenter")){
                        presenterRoleFlag = true;
                        presenterlist.add(tempUser);
                    }
                    presenterRoleFlag = false;
                }   
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReviewSelectScheduleProgramService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return presenterlist;
    }
    
    public List<User> displayProducer(){
        List<User> producerlist = new ArrayList();
        List<User> tempList = new ArrayList();
        Boolean producerRoleFlag = false;
        try{
            tempList = udao.loadAll();
            int userListLength = tempList.size();
            for(int i = 0; i < userListLength; i++){
                User tempUser = tempList.get(i);
                List tempRole = tempUser.getRoles();
                for(int j = 0; j < tempRole.size(); j++){
                    String role = tempUser.getRoles().get(j).getRole();
                    if(role.equals("producer")){
                        producerRoleFlag = true;
                        producerlist.add(tempUser);
                    }
                    producerRoleFlag = false;
                }   
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReviewSelectScheduleProgramService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return producerlist;
    }
}
