package sg.edu.nus.iss.phoenix.schedule.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * AnnualSchedule Value Object. This class is value object representing database table
 * annual-schedule This class is intended to be used together with associated Dao object.
 * 
 * @author Kevin
 */
public class AnnualSchedule implements Cloneable, Serializable {
    
    private String assignedBy;
    private int year;
    private ArrayList<WeeklySchedule> weeklySchedules = new ArrayList<WeeklySchedule>();
    
    /**
     * This constructor creates an instance of AnnualSchedule object
     */
    public AnnualSchedule() {
        
    }
    
    /**
     * This constructor creates an instance of AnnualSchedule object
     * @param year the annual schedule year
     */
    public AnnualSchedule(int year) {
        this.year = year;
    }

    /**
     * This method returns the annual schedule creator
     * @return the annual schedule creator
     */
    public String getAssignedBy() {
        return assignedBy;
    }

    /**
     * This method set the annual schedule creator
     * @param assignedBy the annual schedule creator
     */
    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    /**
     * This method returns the annual schedule year
     * @return the annual schedule year
     */
    public int getYear() {
        return year;
    }

    /**
     * This method set the annual schedule year
     * @param year the annual schedule year
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * This method returns the list of weekly schedules within the annual schedule
     * @return the weekly schedules inside the annual schedule
     */
    public ArrayList<WeeklySchedule> getWeeklySchedules() {
        return weeklySchedules;
    }

    /**
     * This method set the list of weekly schedules within the annual schedule
     * @param weeklySchedules the weekly schedule of the annual schedule
     */
    public void setWeeklySchedules(ArrayList<WeeklySchedule> weeklySchedules) {
        this.weeklySchedules = weeklySchedules;
    }
}
