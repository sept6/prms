package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.delegate.ReviewSelectScheduleProgramDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 * This class is a controller for search radio program schedule.
 * 
 * @author Kevin
 */
@Action("searchsch")
public class SearchRadioProgramScheduleCmd implements Perform {

    /**
     * This method perform input format validation and 
     * invoke reviewSelectRadioProgram method
     * 
     * @param path forward url path
     * @param req HttpServlet request
     * @param resp HttpServlet response
     * @throws ServletException throw servlet exception 
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String startDateInput = req.getParameter("startDate");
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
        // Validate start date search parameter not empty
        if (startDateInput.isEmpty()) {
            req.setAttribute("error", "Please specify start date in search criteria");
            return "/pages/crudsch.jsp";
        }
        
        try {
            Date startDate = df.parse(startDateInput);
        
            ReviewSelectScheduleProgramDelegate del = new ReviewSelectScheduleProgramDelegate();
            List<ProgramSlot> data = del.reviewSelectRadioProgram(startDate);
            req.setAttribute("rps", data);
            req.getSession().setAttribute("ScheduleProgramSlot", data);
            return "/pages/crudsch.jsp";
        } catch (ParseException ex) {
            Logger.getLogger(SearchRadioProgramScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
            req.setAttribute("error", "Invalid date format");
            return "/pages/crudsch.jsp";
        }        
    }
    
}
