package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;

/**
 * This class is a controller for create annual / weekly schedule.
 * 
 * @author Kevin
 */
@Action("createannualweeklysch")
public class CreateAnnualWeeklyScheduleCmd implements Perform {
    
    /**
     * This method perform input format validation and 
     * invoke processCreateAnnualWeeklySchedule method
     * 
     * @param path forward url path
     * @param req HttpServlet request
     * @param resp HttpServlet response
     * @throws ServletException throws servlet exception
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String annualschyear = req.getParameter("annualschyear");
        
        if (validateFormat(annualschyear) != "") {
            req.setAttribute("error", validateFormat(annualschyear));
            return "/pages/setupannualweeklysch.jsp";
        }
        
        ScheduleDelegate schDelegate = new ScheduleDelegate();
        User usr = (User)req.getSession().getAttribute("user");
        AnnualSchedule annualSch = new AnnualSchedule();
        annualSch.setYear(Integer.parseInt(annualschyear));
        annualSch.setAssignedBy(path);
        annualSch.setAssignedBy(usr.getId());        
        
        String error = schDelegate.processCreateAnnualWeeklySchedule(annualSch);
        
        if (error != "") {
            req.setAttribute("error", error);
        } else {
            req.setAttribute("message", "Annual/Weekly schedule sucesfully created");
        }
        
        return "/pages/setupannualweeklysch.jsp";
    }
    
    /**
     * This method validate the format of annual schedule year
     * 
     * @param annualschyear annual schedule year
     * @return error message
     */
    private String validateFormat(String annualschyear) {
        try {
            Integer.parseInt(annualschyear);
        } catch (NumberFormatException e) {
            return "Invalid year";
        }        
        return "";
    }
}
