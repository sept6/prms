/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.delegate;

import java.util.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.schedule.service.ReviewSelectPresenterProducerService;
/**
 *
 * @author HuangXin
 */
public class ReviewSelectPresenterProducerDelegate {
    public List<User> reviewPresenter(){
        ReviewSelectPresenterProducerService rsppService = new ReviewSelectPresenterProducerService();
        List<User> presenterList = rsppService.displayPresenter();
        return presenterList;
    }
    
    public List<User> reviewproducer(){
        ReviewSelectPresenterProducerService rsppService = new ReviewSelectPresenterProducerService();
        List<User> producerList = rsppService.displayProducer();
        return producerList;
    }
}