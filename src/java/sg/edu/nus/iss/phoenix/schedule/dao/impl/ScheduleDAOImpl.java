package sg.edu.nus.iss.phoenix.schedule.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import sg.edu.nus.iss.phoenix.core.dao.DBConstants;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;

/**
 * Schedule Data Access Object (DAO). This class contains all database handling
 * that is needed to permanently store and retrieve ProgramSlot object
 * instances.
 *
 * @author Kevin
 */
public class ScheduleDAOImpl implements ScheduleDAO {

    Connection connection;

    /**
     * This method will search program slot for the next 1 week (7 days) from
     * the specified start date
     *
     * @param startDate program start date
     * @return list of program slot
     * @throws NotFoundException throw not found exception if program slot not fund
     * @throws SQLException throw SQL exception if database execution error
     */
    @Override
    public List<ProgramSlot> retrieveProgramSlot(Date startDate)
            throws NotFoundException, SQLException {

        if (startDate == null) {
            throw new NotFoundException("Cannot select without schedule start date!");
        }

        openConnection();

        String sql = "SELECT * FROM `program-slot` WHERE (`dateOfProgram` >= ? ) AND (`dateOfProgram` < ? ) ORDER BY `dateOfProgram` ASC; ";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setDate(1, new java.sql.Date(startDate.getTime()));
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DATE, 7);
        Date endDate = c.getTime();
        stmt.setDate(2, new java.sql.Date(endDate.getTime()));

        List<ProgramSlot> searchResults = listQuery(stmt);
        closeConnection();
        System.out.println("record size" + searchResults.size());
        return searchResults;
    }

    /**
     * databaseQuery-method. This method is a helper method for internal use. It
     * will execute all database queries that will return multiple rows. The
     * resultset will be converted to the List of valueObjects. If no rows were
     * found, an empty List will be returned.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @return list of program slot
     * @throws java.sql.SQLException throw SQL exception if database execution
     * error
     */
    protected List<ProgramSlot> listQuery(PreparedStatement stmt) throws SQLException {

        ArrayList<ProgramSlot> searchResults = new ArrayList<>();
        ResultSet result = null;
        openConnection();
        try {
            result = stmt.executeQuery();

            while (result.next()) {
                ProgramSlot temp = new ProgramSlot();

                temp.setDuration(result.getTime("duration"));
                temp.setDateOfProgram(result.getTimestamp("dateOfProgram"));
                temp.setStartTime(result.getTimestamp("startTime"));
                temp.setAssignedBy(result.getString("assignedBy"));
                temp.setRadioProgram(new RadioProgram(result.getString("program-name")));
                temp.setPresenter(new User(result.getString("presenter")));
                temp.setProducer(new User(result.getString("producer")));

                searchResults.add(temp);
            }

        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }

        return (List<ProgramSlot>) searchResults;
    }

    /**
     * Method to check if particular annual schedule has already exist
     *
     * @param year Annual Schedule year to be checked
     * @return boolean true if annual schedule exits, else return false
     * @throws SQLException throw SQL exception if database execution error
     */
    @Override
    public boolean isAnnualScheduleExist(int year) throws SQLException {
        String sql = "SELECT count(*) FROM `annual-schedule` WHERE (`year` = ? )";
        PreparedStatement stmt = null;
        ResultSet result = null;
        int allRows = 0;
        openConnection();
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, year);
            result = stmt.executeQuery();

            if (result.next()) {
                allRows = result.getInt(1);
            }
        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
        if (allRows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to store annual schedule and weekly schedule into database
     *
     * @param annualSch annual schedule object
     * @throws SQLException throw SQL exception if database execution error
     */
    @Override
    public void createAnnualWeeklySchedule(AnnualSchedule annualSch) throws SQLException {
        String sqlInsertAnnualSchedule = "INSERT INTO `annual-schedule` (`year`, `assingedBy`) VALUES (?,?);";
        String sqlInsertWeeklySchedule = "INSERT INTO `weekly-schedule` (`startDate`, `assignedBy`) VALUES (?,?);";
        PreparedStatement stmt = null;
        openConnection();
        try {
            connection.setAutoCommit(false);
            stmt = connection.prepareStatement(sqlInsertAnnualSchedule);
            stmt.setInt(1, annualSch.getYear());
            stmt.setString(2, annualSch.getAssignedBy());
            databaseUpdate(stmt);

            for (WeeklySchedule w : annualSch.getWeeklySchedules()) {
                stmt = connection.prepareStatement(sqlInsertWeeklySchedule);
                stmt.setDate(1, new java.sql.Date(w.getStartDate().getTime()));
                stmt.setString(2, w.getAssignedBy());
                databaseUpdate(stmt);
            }

            connection.commit();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    private void openConnection() {
        try {
            Class.forName(DBConstants.COM_MYSQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            this.connection = DriverManager.getConnection(DBConstants.dbUrl,
                    DBConstants.dbUserName, DBConstants.dbPassword);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * create-method. Insert program slot into the schedule defined
     * 
     * @param programSlot details of program slot
     * @return boolean true or false
     * @throws java.sql.SQLException throw SQL exception if database execution error
     */
    @Override
    public Boolean create(ProgramSlot programSlot) throws SQLException {

        Boolean result = false;

        String sql = "insert into `program-slot`(`duration`,`dateofprogram`,`starttime`,`program-name`,`assignedby`,`presenter`,`producer`)values(?,?,?,?,?,?,?);";
        PreparedStatement stmt = null;
        openConnection();

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setTime(1, programSlot.getDuration());
            stmt.setTimestamp(2, new Timestamp(programSlot.getDateOfProgram().getTime()));
            stmt.setTimestamp(3, new Timestamp(programSlot.getStartTime().getTime()));
            stmt.setString(4, programSlot.getRadioProgram().getName());
            stmt.setString(5, programSlot.getAssignedBy());
            stmt.setString(6, programSlot.getPresenter().getId());
            stmt.setString(7, programSlot.getProducer().getId());

            int rowcount = databaseUpdate(stmt);
            if (rowcount != 1) {
                // System.out.println("PrimaryKey Error when updating DB!");
                throw new SQLException("Error occur when inserting program slot to DB!");
            } else {
                result = true;
            }

        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
        
        return result;
    }

    /**
     * databaseUpdate-method. This method is a helper method for internal use.
     * It will execute all database handling that will change the information in
     * tables. SELECT queries will not be executed here however. The return
     * value indicates how many rows were affected. This method will also make
     * sure that if cache is used, it will reset when data changes.
     *
     * @param stmt database prepare statement This parameter contains the SQL
     * statement to be executed.
     * @return integer after database update
     * @throws java.sql.SQLException throw SQL exception if database execution
     * error
     */
    protected int databaseUpdate(PreparedStatement stmt) throws SQLException {
        int result = stmt.executeUpdate();
        return result;
    }

    /**
     * Method to check if particular time slot has already exist
     *
     * @param dateProgramTime program start date and time
     * @return boolean true if time slot found, else false
     * @throws SQLException throw SQL exception if database execution error
     */
    @Override
    public boolean isTimeSlotAvailable(Timestamp dateProgramTime) throws SQLException {
        String sql = "SELECT count(*) FROM `program-slot` WHERE (`dateofprogram` <= ? and ADDTIME(dateofprogram,duration) > ?)";
        PreparedStatement stmt = null;
        ResultSet result = null;
        int allRows = 0;
        openConnection();
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setTimestamp(1, dateProgramTime);
            stmt.setTimestamp(2, dateProgramTime);
            result = stmt.executeQuery();

            if (result.next()) {
                allRows = result.getInt(1);
            }
        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
        if (allRows > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Delete program slot.
     * 
     * @param programSlot details of program slot.
     * @throws NotFoundException throw not found exception if the program slot is not found from database
     * @throws SQLException throw SQL exception
     * @return boolean true if delete successfully, else return false
     */
    @Override
    public boolean delete(ProgramSlot programSlot) throws NotFoundException, SQLException {
        String sql = "delete from `program-slot` where `starttime`=?";
         PreparedStatement stmt = null;
         openConnection();
        
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(programSlot.getStartTime().getTime()));
            int rowcount = databaseUpdate(stmt);
            if (rowcount != 1) {
                // System.out.println("PrimaryKey Error when updating DB!");
                throw new SQLException("Error occur when inserting program slot to DB!");
            }
            if (rowcount > 1) {
                return true;
            }
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
        return false;
    }

    /**
     * RetrieveProgramSlot-method. This method will retrieve program slot based on
     * ProgramSlot object value as a search criteria
     * 
     * @param programSlot value object as a search criteria
     * @return list of program slot
     * @throws NotFoundException throw exception if no result found
     * @throws SQLException throw SQL exception if database execution error
     */
    @Override
    public List<ProgramSlot> findProgram(ProgramSlot programSlot) throws NotFoundException, SQLException {
        openConnection();

        //"insert into `program-slot`(`duration`,`dateofprogram`,`starttime`,`program-name`,`assignedby`,`presenter`,`producer`)values(?,?,?,?,?,?,?);"
        //stmt.setTimestamp(2, new Timestamp(programSlot.getDateOfProgram().getTime()));
        //stmt.setTimestamp(3, new Timestamp(programSlot.getStartTime().getTime()));
        StringBuilder sb = new StringBuilder("SELECT * FROM `program-slot` WHERE ");
        if (programSlot.getRadioProgram() != null) {
            sb.append("`program-name` = ")
                    .append("'")
                    .append(programSlot.getRadioProgram().getName())
                    .append("'")
                    .append(" AND ");
        }
        if (programSlot.getAssignedBy() != null) {
            sb.append("`assignedby` = ")
                    .append("'")
                    .append(programSlot.getAssignedBy())
                    .append("'")
                    .append(" AND ");
        }
        if (programSlot.getPresenter() != null) {
            sb.append("`presenter` = ")
                    .append("'")
                    .append(programSlot.getPresenter().getId())
                    .append("'")
                    .append(" AND ");
        }
        if (programSlot.getProducer() != null) {
            sb.append("`producer` = ")
                    .append("'")
                    .append(programSlot.getProducer().getId())
                    .append("'")
                    .append(" AND ");
        }
        String sql = sb.toString();
        sql = sql.substring(0, sb.length() - 4);
        sql = sql + "ORDER BY `dateOfProgram` ASC; ";

        PreparedStatement stmt = connection.prepareStatement(sql);

        List<ProgramSlot> searchResults = listQuery(stmt);
        closeConnection();
        System.out.println("record size" + searchResults.size());
        return searchResults;
    }
}
