package sg.edu.nus.iss.phoenix.schedule.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

public interface ScheduleDAO {
    
    /**
     * RetrieveProgramSlot-method. This will read all contents from database table and build
     * a List containing valueObjects. Please note, that this method will
     * consume huge amounts of resources if table has lot's of rows. This should
     * only be used when target tables have only small amounts of data.
     * 
     * @param startDate program slot start date
     * @return list of program slot
     * @throws sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException throw exception if no result found
     * @throws java.sql.SQLException throw SQL exception if database execution error
     */
    public abstract List<ProgramSlot> retrieveProgramSlot(Date startDate) 
            throws NotFoundException, SQLException;
    
    /**
     * RetrieveProgramSlot-method. This method will retrieve program slot based on
     * ProgramSlot object value as a search criteria
     * 
     * @param programSlot value object as a search criteria
     * @return list of program slot
     * @throws NotFoundException throw exception if no result found
     * @throws SQLException throw SQL exception if database execution error
     */
    public abstract List<ProgramSlot> findProgram(ProgramSlot programSlot) 
            throws NotFoundException, SQLException;
    
     /**
     * create-method. Insert program slot into the schedule defined
     * 
     * @param programSlot details of program slot
     * @return boolean true or false
     * @throws sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException throw exception if no result found
     * @throws java.sql.SQLException throw SQL exception if database execution error
     */
    public Boolean create(ProgramSlot programSlot) 
            throws NotFoundException, SQLException;
    
    /**
     * Function to check if particular annual schedule has already exist
     * @param year Annual Schedule year to be checked
     * @return boolean true if annual schedule exits, else false
     * @throws SQLException throw SQL exception
     */
    public boolean isAnnualScheduleExist(int year)
            throws SQLException;
    
    /**
     * Method to store annual schedule and weekly schedule into database
     * @param annualSch annual schedule object
     * @throws SQLException throw SQL exception
     */
    public void createAnnualWeeklySchedule(AnnualSchedule annualSch)
            throws SQLException;
    
     /**
     * Method to check whether the specific time slot is available
     * @param dateProgramTime program start date and time
     * @throws SQLException throw SQL exception
     * @return boolean true if annual schedule exits, else false
     */
    public boolean isTimeSlotAvailable(Timestamp dateProgramTime) throws SQLException;
    
    /**
     * Delete program slot.
     * 
     * @param programSlot details of program slot.
     * @throws NotFoundException throw not found exception if the program slot is not found from database
     * @throws SQLException throw SQL exception
     * @return boolean true if delete successfully, else return false
     */
    public boolean delete(ProgramSlot programSlot) 
            throws NotFoundException, SQLException;
}
