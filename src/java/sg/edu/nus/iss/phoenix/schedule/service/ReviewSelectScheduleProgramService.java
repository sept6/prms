package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.radioprogram.dao.ProgramDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 *
 * @author Kevin
 */
public class ReviewSelectScheduleProgramService {
    DAOFactoryImpl factory;
    ScheduleDAO schdao;
    ProgramDAO rpdao;
    UserDao udao;
    
    public ReviewSelectScheduleProgramService() {
        super();
        factory = new DAOFactoryImpl();
        schdao = factory.getScheduleDAO();
        rpdao = factory.getProgramDAO();
        udao = factory.getUserDAO();
    }
    
    public List<ProgramSlot> reviewSelectScheduleProgram(Date startDate) {
        List<ProgramSlot> list = null;
        try { 
            list = schdao.retrieveProgramSlot(startDate);
            
            // Load related Radio Program, Presenter, and Producer
            for (ProgramSlot s:list) {
                rpdao.load(s.getRadioProgram());
                udao.load(s.getPresenter());
                udao.load(s.getProducer());
            }
        } catch (NotFoundException ex) {
            Logger.getLogger(ReviewSelectScheduleProgramService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReviewSelectScheduleProgramService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
