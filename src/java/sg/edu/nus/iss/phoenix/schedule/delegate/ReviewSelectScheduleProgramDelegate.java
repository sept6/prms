package sg.edu.nus.iss.phoenix.schedule.delegate;

import java.util.Date;
import java.util.List;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.service.ReviewSelectScheduleProgramService;

/**
 *
 * @author Kevin
 */
public class ReviewSelectScheduleProgramDelegate {
    
    public List<ProgramSlot> reviewSelectRadioProgram(Date startDate) {
        ReviewSelectScheduleProgramService service = new ReviewSelectScheduleProgramService();
        return service.reviewSelectScheduleProgram(startDate);	
    }
    
}
