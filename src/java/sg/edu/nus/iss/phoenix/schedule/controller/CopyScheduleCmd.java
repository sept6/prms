/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 *
 * @author andy
 */
@Action("copysch")
public class CopyScheduleCmd implements Perform{

     /**
	 * perform. This will redirect to copy schedule page
	 * 
	 *
     * @param path Redirect path
     * @param req  HttpRequest
     * @param resp HttpResponse
     * @return copy schedule page
     * @throws java.io.IOException throw IO exception
     * @throws javax.servlet.ServletException throw servlet exception
     * @author andy
	 */
    
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
       
        String[] checkboxValues = req.getParameterValues("name"); 
        List<ProgramSlot> selectedProgramSlot = new ArrayList<ProgramSlot>();
        
        if (checkboxValues == null) {
            req.setAttribute("error", "Please select at least one program slot to copy.");
            return "/pages/crudsch.jsp";
        }
         
        if(req.getSession().getAttribute("ScheduleProgramSlot") != null){
            List<ProgramSlot> data = ( List<ProgramSlot>)req.getSession().getAttribute("ScheduleProgramSlot");
            for(String chkBoxValue: checkboxValues){
                  for (ProgramSlot slot : data) {
                     Date programDate = Utility.toDate(chkBoxValue,Utility.DATE_TIME_PATTERN1);
                    if(slot.getDateOfProgram() != null && slot.getDateOfProgram().compareTo(programDate) == 0){
                        selectedProgramSlot.add(slot);
                    }
                 }
            }
            req.setAttribute("copyrps", selectedProgramSlot);
            req.getSession().setAttribute("selectedProgramSlot", selectedProgramSlot);
        }
        
        return "/pages/copysch.jsp";
    }
    
}
