/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.schedule.delegate.ReviewSelectScheduleProgramDelegate;
import sg.edu.nus.iss.phoenix.schedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 * Command for delete schedule(program slot).
 * @author Kyaw Min Thu
 */
@Action("deletesch")
public class DeleteScheduleCmd implements Perform {
    
    /**
     * This method invoke processDelete method
     * 
     * @param path forward url path
     * @param req HttpServlet request
     * @param resp HttpServlet response
     * @throws ServletException throw servlet exception and IOException.
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ScheduleDelegate scheduleDelegate = new ScheduleDelegate();
        ProgramSlot programSlot = new ProgramSlot();
        
        String strDateOfProgram = req.getParameter("dateOfProgram");
        
        String[] strDateOfProgramArr = strDateOfProgram.split(" ");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //Time time = new Time(duration);
        Date dateOfProgram = null;
        Date timeOfProgram = null;
        try {
            dateOfProgram = dateFormat.parse(strDateOfProgramArr[0]);
            timeOfProgram = dateFormatTime.parse(strDateOfProgram);
            programSlot.setStartTime(timeOfProgram);
            programSlot.setDateOfProgram(dateOfProgram);
            String error = scheduleDelegate.processDelete(programSlot);
            
            if (error != null) {
                System.out.println("Schedule deletion not successful.");
                List<String> errorList = new ArrayList();
                errorList.add(error.trim());
                req.setAttribute("errorList", errorList);
            } else {
                System.out.println("Successfully Deleted.");
                req.setAttribute("message", "message.deletechedulesucess");
            }
        } catch (ParseException ex) {
            Logger.getLogger(CreateScheduleCmd.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Parse Exception: " + ex.getMessage());
            req.setAttribute("message", "error.deleteschedulefailed");
            return "/pages/error.jsp";
        }
        ReviewSelectScheduleProgramDelegate rsSchedule = new ReviewSelectScheduleProgramDelegate();
        List<ProgramSlot> data = rsSchedule.reviewSelectRadioProgram(dateOfProgram);
        req.setAttribute("rps", data);
        return "/pages/crudsch.jsp";
    } 
}
