package sg.edu.nus.iss.phoenix.schedule.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 * ProgramSlot Value Object. This class is value object representing database table
 * program-slot This class is intended to be used together with associated Dao object.
 * 
 * @author Kevin
 */
public class ProgramSlot implements Cloneable, Serializable {
    
    private Date dateOfProgram;
    private Time duration;
    private Date startTime;
    private String assignedBy;
    private RadioProgram radioProgram;
    private User producer;
    private User presenter;
    
    /**
     * This constructor creates an instance of ProgramSlot object
     */
    public ProgramSlot() {
        
    }
    
    /**
     * This constructor creates an instance of ProgramSlot object
     * @param duration the program slot duration
     * @param dateOfProgram the program slot date
     */
    public ProgramSlot(Time duration, Date dateOfProgram) {
        this.duration = duration;
        this.dateOfProgram = dateOfProgram;
    }

    /**
     * This method returns the program slot date
     * @return the program slot date
     */
    public Date getDateOfProgram() {
        return dateOfProgram;
    }

    /**
     * This method set the program slot date
     * @param dateOfProgram the program slot date
     */
    public void setDateOfProgram(Date dateOfProgram) {
        this.dateOfProgram = dateOfProgram;
    }

    /**
     * This method returns the program slot duration
     * @return the program slot duration
     */
    public Time getDuration() {
        return duration;
    }

    /**
     * This method set the program slot duration
     * @param duration the program slot duration
     */
    public void setDuration(Time duration) {
        this.duration = duration;
    }

    /**
     * This method returns the program slot start time
     * @return the program slot start time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * This method set the program slot start time
     * @param startTime the program slot start time
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * This method returns the program slot creator
     * @return the program slot creator
     */
    public String getAssignedBy() {
        return assignedBy;
    }

    /**
     * This method set the program slot creator
     * @param assignedBy the program slot creator
     */
    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    /**
     * This method returns the radio program of the program slot
     * @return the radio program of the program slot
     */
    public RadioProgram getRadioProgram() {
        return radioProgram;
    }

    /**
     * This method set the radio program of the program slot
     * @param radioProgram the radio program of the program slot
     */
    public void setRadioProgram(RadioProgram radioProgram) {
        this.radioProgram = radioProgram;
    }
    
    /**
     * This method returns the program slot producer
     * @return the program slot producer
     */
    public User getProducer() {
        return producer;
    }

    /**
     * This method set the program slot producer
     * @param producer the program slot producer
     */
    public void setProducer(User producer) {
        this.producer = producer;
    }

    /**
     * This method returns the program slot presenter
     * @return the program slot presenter
     */
    public User getPresenter() {
        return presenter;
    }

    /**
     * This method set the program slot presenter
     * @param presenter the program slot presenter
     */
    public void setPresenter(User presenter) {
        this.presenter = presenter;
    } 
    
}
