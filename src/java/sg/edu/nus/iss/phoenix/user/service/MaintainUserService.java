/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDaoImpl;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactory;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.impl.ScheduleDAOImpl;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 * This class is a service for maintain user functionalities. The maintain user 
 * requests such as create ,modify and delete user record from the commands 
 * are handled in this class.
 * 
 * @author Raghavendra S
 */
public class MaintainUserService {
    
    private DAOFactory factory;
    private UserDao udao;
    private ScheduleDAO sdao;
    
    /**
     * Creates maintain user instance and initialize required attributes.
     */
    public MaintainUserService(){
        super();
        factory = new DAOFactoryImpl();
        udao = new UserDaoImpl();
        sdao = new ScheduleDAOImpl();
    }

    /**
     * Gets the DAOFactory instance.
     * 
     * @return DAOFactoryImpl instance.
     */
    public DAOFactory getFactory() {
        return factory;
    }

    /**
     * Sets the DAOFactory instance.
     * 
     * @param factory, an instance of DAO factory.
     */
    public void setFactory(DAOFactory factory) {
        this.factory = factory;
    }

    /**
     * Returns the UserDAO instance.
     * 
     * @return UserDAO instance.
     */
    public UserDao getUdao() {
        return udao;
    }

    /**
     * Sets the UserDAO implementation instance.
     * 
     * @param udao - UserDAO implementation instance.
     */
    public void setUdao(UserDao udao) {
        this.udao = udao;
    }

    /**
     * Returns ScheduleDAO implementation instance.
     * @return ScheduleDAO implementation.
     */
    public ScheduleDAO getSdao() {
        return sdao;
    }

    /**
     * Sets the ScheduleDAO implementation instance.
     * 
     * @param sdao - ScheduleDAO implementation instance.
     */
    public void setSdao(ScheduleDAO sdao) {
        this.sdao = sdao;
    }
    
    /**
     * Invokes the create method of UserDAO and create the user record.
     * 
     * @param user user object contain user information
     * @throws java.sql.SQLException throw SQL exception if database execution failed
     */
    public void processCreate(User user) throws SQLException {
            try {
                    udao.create(user);
            } catch (SQLException e) {
                Logger.getLogger(MaintainUserService.class.getName()).log(Level.SEVERE, null, e);
                throw e;
            }
    }

    /**
     * Modify user record.
     * 
     * @param user, the record to be modified.
     * @return null, If the process is successful, otherwise, return error message.
     * @throws NotFoundException throw not found exception if user not found in database
     * @throws SQLException throw SQL exception if database execution failed
     */
    public String processModify(User user) throws NotFoundException, SQLException{
        try {
            if (Utility.isEmpty(user.getName())
                    || Utility.isEmpty(user.getRoles())
                    || Utility.isEmpty(user.getRoles())) {
                return "error.incompletedataforusrmodify";
            }
            String newRoles = user.getRoles().get(0).getRole();
                
            User oldUser = new User(user.getId());
            udao.load(oldUser);
            
            if (isUserRoleChanged(oldUser, Utility.createRoles(newRoles))) {
                if (!deleteOldRole(oldUser, newRoles)) {
                    return "error.edituserhaspendingschedule";
                }
            }
            udao.save(user);
        } catch (NotFoundException e) {
                Logger.getLogger(MaintainUserService.class.getName()).log(Level.SEVERE, null, e);
                throw e;
        } catch (SQLException e) {
                Logger.getLogger(MaintainUserService.class.getName()).log(Level.SEVERE, null, e);
                throw e;
        }
        return null;
    }
    
    private boolean deleteOldRole(User oldUser, String newRoles) throws NotFoundException, SQLException {
        for (Role role : oldUser.getRoles()) {
            if ((role.getRole().contains("presenter") && !newRoles.contains("presenter")) 
                    || (role.getRole().contains("producer") && !newRoles.contains("producer"))) {
                ProgramSlot programSlot = new ProgramSlot();
                if (role.getRole().contains("presenter")) {
                    programSlot.setPresenter(oldUser);
                }
                if (role.getRole().contains("producer")) {
                    programSlot.setProducer(oldUser);
                }
                List<ProgramSlot> assignedProgramSlots = sdao.findProgram(programSlot);
                
                // If old role is Presenter or Producer, check user has assigned for pending radio program.
                if (assignedProgramSlots != null) {
                    for (ProgramSlot ps : assignedProgramSlots) {
                        if (ps.getStartTime().after(Calendar.getInstance().getTime())) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    
    private boolean isUserRoleChanged(User oldUser, List<Role> roles) {
        System.out.println("oldUser roles = " + oldUser.getRoles());
        System.out.println("user roles = " + roles);
        if (oldUser.getRoles() == null || roles == null) {
            if (oldUser.getRoles() != roles) {
                return true;
            } else {
                return false;
            }
        }
        if (oldUser.getRoles().size() != roles.size()) {
            return true;
        } else {
            for (Role role : roles) {
                if (!oldUser.getRoles().contains(role)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Deletes the user record for the specified user with provided id. 
     * The method verifies if the user record exists or whether the user 
     * is a presenter/producer assigned to a future program slot and returns 
     * appropriate error message. 
     * 
     * @param id, the user's id to be deleted.
     * @return String success message if delete successfully
     * @throws NotFoundException throw not found exception if the user not found in database
     * @throws SQLException throw SQL exception if database execution failed
     */
    public String processDelete(String id) throws NotFoundException, SQLException {

        try {
            User user = new User(id);
            udao.load(user);
            
            ProgramSlot programSlot = new ProgramSlot();
            boolean isPresenterProducer = false;
            if (user.getRoles().contains(new Role("presenter"))) {
                programSlot.setPresenter(user);
                isPresenterProducer = true;
            }
            if (user.getRoles().contains(new Role("producer"))) {
                programSlot.setProducer(user);
                isPresenterProducer = true;
            }
            
            if(isPresenterProducer){
                List<ProgramSlot> assignedProgramSlots = sdao.findProgram(programSlot);
                if (assignedProgramSlots != null) {
                    for (ProgramSlot ps : assignedProgramSlots) {
                        if (ps.getStartTime().after(Calendar.getInstance().getTime())) {
                            return "error.edituserhaspendingschedule";
                        }
                    }
                }
            }
            udao.delete(user);
        } catch (NotFoundException e) {
            Logger.getLogger(MaintainUserService.class.getName()).log(Level.SEVERE, null, e);
            throw e;
        } catch (SQLException e) {
            Logger.getLogger(MaintainUserService.class.getName()).log(Level.SEVERE, null, e);
            throw e;
        }
        return null;
    }
    
    /**
     * Checks if the user record exists.
     * 
     * @param name, name of the user to verify.
     * @return boolean true if user exits, else false
     * @throws SQLException throw SQL exception if database execution failed
     */
    public boolean isUserExists(String name) throws SQLException{
        
        if(udao.searchMatching(name)!=null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns all the user records available in the system. 
     *  
     * @return The list of users.
     * @throws SQLException throw SQL exception if database execution failed
     */
    public List<User> getUserList() throws SQLException {
        return udao.loadAll();
    }
}
