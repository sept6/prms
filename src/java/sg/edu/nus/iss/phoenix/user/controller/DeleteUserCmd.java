/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.user.delegate.MaintainUserDelegate;

/**
 * This command for delete user. Delete user requests are received and delegated 
 * to <code>MaintainUserDelegate</code>.
 * 
 * @author Raghavendra S
 */
@Action("deleteusr")
public class DeleteUserCmd implements Perform {

    /**
     * Delegates the delete user request to <code>MaintainUserDelegate</code> 
     * after basic validation.
     * 
     * @param path forward url path
     * @param hsr HttpServlet request
     * @param hsresp HttpServlet response
     * @throws ServletException throw servlet exception and IOException.
     */
    @Override
    public String perform(String path, HttpServletRequest hsr, HttpServletResponse hsresp) throws IOException, ServletException {
        MaintainUserDelegate userDelegate = new MaintainUserDelegate();
        User user = new User();
        user.setId(hsr.getParameter("id"));
        List<String> errorList = new ArrayList<>();
        try {
            if(userDelegate.isUserExists(user)){
                String error = userDelegate.processDelete(hsr.getParameter("id"));
                if(error == null){
                    hsr.setAttribute("message", "message.deleteusersucess");
                    return "/pages/success.jsp";
                }else{
                    errorList.add(error);
                    hsr.setAttribute("errorList", errorList);
                    return "/pages/customerror.jsp";
                }
            } else {
                hsr.setAttribute("message", "error.user.notexists");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DeleteUserCmd.class.getName()).log(Level.SEVERE, null, ex);   
            hsr.setAttribute("message", "error.deleteuserfailed");
        } catch (NotFoundException ex) {
            Logger.getLogger(DeleteUserCmd.class.getName()).log(Level.SEVERE, null, ex);
            hsr.setAttribute("message", "error.deleteuserfailed");
        }
        
        return "/pages/customerror.jsp"; 
    }
}
