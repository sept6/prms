/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.delegate.MaintainUserDelegate;

/**
 * This class is a controller for create user request, which receives 
 * the create requests, perform basic validation and invokes the <code>processCreate()</code>
 * method of the <code>MaintainUserDelegate</code> to create the user record with the 
 * provided attributes as part of the request.
 * 
 * @author Raghavendra S
 */
@Action("createuser")
public class CreateUserCmd implements Perform {

    
    /**
     * This method perform basic validation of the parameters submitted as part of the request
     * and invokes the <code>processCreate()</code> method of the <code>MaintainUserDelegate</code> 
     * to create a user record with submitted attributes.
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsresp) throws IOException, ServletException {
        
        String name = hsr.getParameter("name");
        String id = hsr.getParameter("id");
        String password = hsr.getParameter("password");
        String role = hsr.getParameter("role");
        List<String> errorList = new ArrayList();
        
        if(name==null || name.trim().equals("")){
            errorList.add("error.invalidname");
        } 
        if(id==null || id.trim().equals("") || id.length() < 8){
            errorList.add("error.invalidlogonid");
        }        
        if(password==null || password.trim().equals("") || password.length() < 6){
            errorList.add("error.invalidpassword");
        }         
        if(role==null || role.trim().equals("")){
            errorList.add("error.invalidrole");
        }  
        if (errorList.size() > 0) {
            
            hsr.setAttribute("message", "error.createuserfailed");
            hsr.setAttribute("errorList", errorList);
            return "/pages/customerror.jsp";
        }
        MaintainUserDelegate userDelegate = new MaintainUserDelegate();
        User user = new User();
        user.setAll(id, password, name, role);
        try {
            if(!userDelegate.isUserExists(user)){
                userDelegate.processCreate(user);
                hsr.setAttribute("message", "message.createusersucess");
                return "/pages/success.jsp";
            } else {
                hsr.setAttribute("message", "error.createuserfailed");
                errorList.add("error.userexists");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateUserCmd.class.getName()).log(Level.SEVERE, null, ex);
        }
        hsr.setAttribute("errorList", errorList);
        return "/pages/customerror.jsp";
       
    }
    
}
