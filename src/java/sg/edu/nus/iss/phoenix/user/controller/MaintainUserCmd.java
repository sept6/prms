/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.delegate.MaintainUserDelegate;

/**
 *
 * @author andy, Raghavendra
 * 
 */
@Action("manageuser")
public class MaintainUserCmd implements Perform{

    /**
      * perform. This will redirect to maintain user page
      * 
      *
     * @return maintain user page
     * @throws java.io.IOException throw IO exception
     * @throws javax.servlet.ServletException throw servlet.servlet exception
     * 
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        MaintainUserDelegate del = new MaintainUserDelegate();
        List<User> userList =null;
        try {
            userList = del.getUserList();
        } catch (SQLException ex) {
            Logger.getLogger(MaintainUserCmd.class.getName()).log(Level.SEVERE.SEVERE, null, ex);
        }
        hsr.setAttribute("users", userList);
        return "/pages/cruduser.jsp";
    }
    
}
