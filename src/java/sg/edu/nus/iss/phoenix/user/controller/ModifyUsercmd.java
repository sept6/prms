/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import com.mysql.jdbc.Util;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.delegate.MaintainUserDelegate;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 * Command for modify user.
 * @author Kyaw Min Thu
 */
@Action("modifyuser")
public class ModifyUsercmd implements Perform {
    
    /**
     * This method perform input format validation and 
     * invoke processModify method
     * 
     * @param path forward url path
     * @param hsr HttpServlet request
     * @param hsr1 HttpServlet response
     * @throws ServletException throw servlet exception and IOException.
     */
    @Override
    public String perform(String path, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        MaintainUserDelegate userDelegate = new MaintainUserDelegate();
        User user = new User();
        System.out.println("hidden id : " + hsr.getParameter("hid"));
        System.out.println("id : " + hsr.getParameter("id"));
        System.out.println("password : " + hsr.getParameter("password"));
        System.out.println("name : " + hsr.getParameter("name"));
        System.out.println("role : " + hsr.getParameter("role"));
        List<String> errorList = new ArrayList();
        
        if(Utility.isEmpty(hsr.getParameter("name"))){
            errorList.add("error.invalidname");
        }      
        if(Utility.isEmpty(hsr.getParameter("password")) 
                || hsr.getParameter("password").length() < 6){
            errorList.add("error.invalidpassword");
        }         
        if(Utility.isEmpty(hsr.getParameter("role"))){
            errorList.add("error.invalidrole");
        }  
        if (errorList.size() > 0) {
            
            hsr.setAttribute("message", "error.modifyuserfailed");
            hsr.setAttribute("errorList", errorList);
            return "/pages/customerror.jsp";
        }
        user.setAll(hsr.getParameter("hid"), hsr.getParameter("password"), hsr.getParameter("name"), hsr.getParameter("role"));
        try {
            if(userDelegate.isUserExists(user)){
                String error = userDelegate.processModify(user);
                if (error == null) {
                    hsr.setAttribute("message", "message.modifyusersucess");
                    return "/pages/success.jsp";
                } else {
                    errorList.add(error);
                    hsr.setAttribute("errorList", errorList);
                    return "/pages/customerror.jsp";
                }
            } else {
                hsr.setAttribute("error", "error.usernotfound");
                return "/pages/customerror.jsp";
            }
        } catch (Exception ex) {
            Logger.getLogger(CreateUserCmd.class.getName()).log(Level.SEVERE, null, ex);
            hsr.setAttribute("error", "error.edituserfailed");
            return "/pages/customerror.jsp";
        }
    }
    
}
