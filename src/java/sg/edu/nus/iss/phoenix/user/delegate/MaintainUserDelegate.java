/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.delegate;

import java.sql.SQLException;
import java.util.List;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.user.service.MaintainUserService;

/**
 * This class delegates the requests from maintain user commands to the service.
 * 
 * @author Raghavendra S
 */
public class MaintainUserDelegate {
    
    private MaintainUserService maintainUserService;

    /**
     * The constructor initializes the required attributes.
     */
    public MaintainUserDelegate() {
        maintainUserService = new MaintainUserService();
    }

    /**
     * Delegates the <code>processCreate</code> request to MaintainUserService.
     * 
     * @param user, to be created
     * @throws SQLException throw SQL exception if database execution error
     */
    public void processCreate(User user) throws SQLException {
    maintainUserService.processCreate(user);

    }

    /**
     * Delegates the <code>processModify</code> request to MaintainUserService.
     * 
     * @param user, the user record to be modified
     * @return string success or error
     * @throws NotFoundException throw not found exception if user does not exits in database
     * @throws SQLException throw SQL exception if database execution error
     */
    public String processModify(User user) throws NotFoundException, SQLException {
        return maintainUserService.processModify(user);
    }

    /**
     * Delegates the <code>processDelete</code> request to MaintainUserService.
     * 
     * @param id, the user's id to be deleted.
     * @return String success or failed
     * @throws NotFoundException throw not found exception if user id not found in database
     * @throws SQLException throw SQL exception if database execution error
     */
    public String processDelete(String id) throws NotFoundException, SQLException {
            return maintainUserService.processDelete(id);
    }

    /**
     * Checks if the user record exists.
     * 
     * @param user, the user record to be checked.
     * @return true if the user record exists, false otherwise.
     * @throws SQLException throw SQL exception if database execution error
     */
    public boolean isUserExists(User user) throws SQLException {
        
        
        return maintainUserService.isUserExists(user.getId());
    }

    /**
     * Retrieves all the user records.
     * 
     * @return list of user records.
     * @throws SQLException throw SQL exception if database execution error
     */
    public List<User> getUserList() throws SQLException {
        
         return maintainUserService.getUserList();
    }
}
