/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Redirects the user to setup user page.
 * 
 * @author Raghavendra S
 */
@Action("addeditusr")
public class AddEditUserCmd implements Perform {

    /**
     * Returns the setup user page to be redirected.
     * 
     * @param path redirect path
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @return path
     * @throws IOException throw IO Exception if IO error
     * @throws ServletException throw Servlet Exception
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        return "/pages/setupusr.jsp";
    } 
}
