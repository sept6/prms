/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.schedule.service.ScheduleService;

/**
 *
 * @author andy
 */
public class Utility {
    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_TIME_PATTERN1 = "yyyy-MM-dd HH:mm:ss";

    private static final String DELIMITER = ":";
    
     public static Date toDate(String txt, String datePattern) {
        if (txt==null || txt.trim().length()==0) {
            return null;
        }
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(datePattern);
            return fmt.parse(txt);
        }
        catch (ParseException ex) {
             Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<Role> createRoles(final String roles) {
            ArrayList<Role> roleList = new ArrayList<>();
            String[] _r = roles.trim().split(DELIMITER);
            for (String r: _r)
                    roleList.add(new Role(r.trim()));
            return (roleList);
    }
    
    public static boolean isEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }
    
    public static boolean isEmpty(Collection list) {
        return list == null || list.isEmpty();
    }
}
