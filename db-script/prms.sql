/*
SQLyog Community v12.01 (64 bit)
MySQL - 5.7.13-0ubuntu0.16.04.2 : Database - phoenix
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phoenix` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `phoenix`;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(40) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `role_index` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`password`,`name`,`role`) values ('catbert','catbert','catbert, Calvin','manager'),('dilbert','dilbert','dilbert, super hero','manager:admin:presenter:producer'),('dogbert','dogbert','dogbert, the CEO','producer:admin'),('kevin1234','kevin1234','kevin','manager:presenter:producer'),('pointyhead','pointyhead','pointyhead, the manager','manager'),('raghavendra','test','Raghavendra','manager'),('testuser10','testpass','Test User 10','manager:presenter:producer'),('testuser3','test','Test User','manager'),('testuser6','testpass','Test6','manager:presenter'),('testuser9','testpass','RaghavendraS','presenter:producer'),('wally','wally','wally, the bludger','producer');

/*Table structure for table `annual-schedule` */

DROP TABLE IF EXISTS `annual-schedule`;

CREATE TABLE `annual-schedule` (
  `year` int(11) NOT NULL,
  `assingedBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`year`),
  KEY `id_annual_schedule` (`assingedBy`),
  CONSTRAINT `id_as` FOREIGN KEY (`assingedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `annual-schedule` */

insert  into `annual-schedule`(`year`,`assingedBy`) values (2016,'pointyhead'),(2017,'pointyhead');

/*Table structure for table `program-slot` */

DROP TABLE IF EXISTS `program-slot`;

CREATE TABLE `program-slot` (
  `duration` time NOT NULL,
  `dateOfProgram` datetime NOT NULL,
  `startTime` datetime DEFAULT NULL,
  `program-name` varchar(45) DEFAULT NULL,
  `assignedBy` varchar(45) DEFAULT NULL,
  `presenter` varchar(40) DEFAULT NULL,
  `producer` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`duration`,`dateOfProgram`),
  UNIQUE KEY `dateOfProgram_UNIQUE` (`dateOfProgram`),
  KEY `name_program_slot` (`program-name`),
  KEY `presenter_idx` (`presenter`),
  KEY `producer_idx` (`producer`),
  CONSTRAINT `name` FOREIGN KEY (`program-name`) REFERENCES `radio-program` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `presenter` FOREIGN KEY (`presenter`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `producer` FOREIGN KEY (`producer`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `program-slot` */

insert  into `program-slot`(`duration`,`dateOfProgram`,`startTime`,`program-name`,`assignedBy`,`presenter`,`producer`) values ('00:05:00','2016-09-30 00:00:00','2016-09-30 19:30:00','short news','raghavendra','testuser6','wally'),('00:05:00','2016-10-27 09:00:00','2016-10-27 09:00:00','short news','raghavendra','dilbert','dilbert'),('00:30:00','2016-01-24 00:09:00','2016-01-24 06:30:00','charity','pointyhead','dilbert','dilbert'),('00:30:00','2016-01-25 00:09:00','2016-01-25 21:30:00','charity','pointyhead','dilbert','dilbert'),('00:30:00','2016-01-29 00:09:00','2016-01-29 09:00:00','dance floor','raghavendra','dilbert','wally'),('00:30:00','2016-09-09 07:30:00','2016-09-09 07:30:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-09 08:00:00','2016-09-09 08:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-09 08:30:00','2016-09-09 08:30:00','noose','pointyhead','dilbert','wally'),('00:30:00','2016-09-15 00:00:00','2016-09-15 04:30:00','opinions','raghavendra','testuser6','wally'),('00:30:00','2016-09-15 03:00:00','2016-09-15 03:00:00','Test','pointyhead','testuser6','dogbert'),('00:30:00','2016-09-18 00:00:00','2016-09-18 12:00:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-18 12:00:00','2016-09-18 12:00:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-18 13:00:00','2016-09-18 13:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-19 18:00:00','2016-09-19 18:00:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-20 02:30:00','2016-09-20 02:30:00','noose','pointyhead','dilbert','dilbert'),('00:30:00','2016-09-20 12:00:00','2016-09-20 12:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-20 14:00:00','2016-09-20 14:00:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-21 02:00:00','2016-09-21 02:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-21 12:00:00','2016-09-21 12:00:00','noose','pointyhead','dilbert','wally'),('00:30:00','2016-09-26 00:00:00','2016-09-26 17:00:00','Test','pointyhead','testuser6','wally'),('00:30:00','2016-09-26 16:00:00','2016-09-26 16:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-26 16:30:00','2016-09-26 16:30:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-26 17:00:00','2016-09-26 17:00:00','news','pointyhead','dilbert','wally'),('00:30:00','2016-09-26 17:30:00','2016-09-26 17:30:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-27 13:30:00','2016-09-27 13:30:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-09-28 00:00:00','2016-09-28 02:00:00','dance floor','raghavendra','testuser6','dogbert'),('00:30:00','2016-09-29 00:00:00','2016-09-29 19:00:00','dance floor','raghavendra','testuser6','dogbert'),('00:30:00','2016-09-30 14:00:00','2016-09-30 14:00:00','ppk','pointyhead','dilbert','dogbert'),('00:30:00','2016-10-03 06:00:00','2016-10-03 06:00:00','noose','pointyhead','testuser6','dogbert'),('00:30:00','2016-10-06 10:00:00','2016-10-06 10:00:00','dance floor','pointyhead','testuser6','dogbert'),('00:30:00','2016-10-08 08:00:00','2016-10-08 08:00:00','charity','pointyhead','dilbert','dilbert'),('00:30:00','2016-10-18 00:00:00','2016-10-18 00:00:00','dance floor','raghavendra','dilbert','testuser10'),('00:30:00','2016-10-18 03:00:00','2016-10-18 03:00:00','opinions','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-19 01:00:00','2016-10-19 01:00:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-20 05:00:00','2016-10-20 05:00:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-21 02:00:00','2016-10-21 02:00:00','opinions','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-22 08:00:00','2016-10-22 08:00:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-25 07:00:00','2016-10-25 07:00:00','noose','raghavendra','testuser9','dilbert'),('00:30:00','2016-10-25 16:00:00','2016-10-25 16:00:00','dance floor','raghavendra','testuser6','kevin1234'),('00:30:00','2016-10-27 01:00:00','2016-10-27 01:00:00','dance floor','raghavendra','testuser6','kevin1234'),('00:30:00','2016-10-27 02:00:00','2016-10-27 02:00:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-27 03:00:00','2016-10-27 03:00:00','dance floor','raghavendra','testuser6','kevin1234'),('00:30:00','2016-10-27 04:00:00','2016-10-27 04:00:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-27 05:00:00','2016-10-27 05:00:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-28 01:00:00','2016-10-28 01:00:00','noose','raghavendra','testuser6','kevin1234'),('00:30:00','2016-10-29 03:30:00','2016-10-29 03:30:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-10-29 09:30:00','2016-10-29 09:30:00','news','raghavendra','dilbert','dilbert'),('00:30:00','2016-11-01 00:00:00','2016-11-01 00:00:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-11-01 00:30:00','2016-11-01 00:30:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-11-01 01:30:00','2016-11-01 01:30:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-11-01 02:00:00','2016-11-01 02:00:00','dance floor','raghavendra','dilbert','dogbert'),('00:30:00','2016-11-02 00:00:00','2016-11-02 00:00:00','dance floor','raghavendra','dilbert','dilbert'),('00:30:00','2016-11-02 00:30:00','2016-11-02 00:30:00','dance floor','raghavendra','dilbert','dilbert'),('00:40:00','2016-09-18 00:30:00','2016-09-18 00:30:00','opinions','pointyhead','testuser6','dilbert'),('00:40:00','2016-09-18 02:00:00','2016-09-18 02:00:00','charity','pointyhead','dilbert','dilbert'),('00:40:00','2016-10-01 09:30:00','2016-10-01 09:30:00','charity','pointyhead','dilbert','dilbert'),('01:00:00','2016-01-15 00:09:00','2016-01-15 14:00:00','your choice','raghavendra','dilbert','dogbert'),('01:00:00','2016-01-28 00:09:00','2016-01-28 15:00:00','your choice','raghavendra','dilbert','wally'),('01:00:00','2016-01-30 00:09:00','2016-01-30 05:00:00','top 10','raghavendra','dilbert','wally'),('01:00:00','2016-09-04 20:00:00','2016-09-04 20:00:00','top 10','pointyhead','dilbert','dilbert'),('01:00:00','2016-09-28 23:00:00','2016-09-28 23:00:00','your choice','pointyhead','dilbert','dilbert'),('01:00:00','2016-12-31 01:30:00','2016-12-31 01:30:00','top 10','raghavendra','testuser6','dogbert');

/*Table structure for table `radio-program` */

DROP TABLE IF EXISTS `radio-program`;

CREATE TABLE `radio-program` (
  `name` varchar(45) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `typicalDuration` time DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `radio-program` */

insert  into `radio-program`(`name`,`desc`,`typicalDuration`) values ('charity','president charity show for unfortunate','00:35:00'),('dance floor','dance show','00:30:00'),('news','full news broadcasted four times a day','00:30:00'),('noose','black comedy show','00:30:00'),('opinions','discuss, debate or share opinions regarding a theme or subject','00:30:00'),('ppk','phu chu kang comedy show','00:30:00'),('short news','summarised 5 minutes broadcasted every 2 hours','00:05:00'),('Test','test program','00:30:00'),('top 10','countdown music play of top 10 songs of the week','01:00:00'),('your choice','audinece ask for music album song of thier choice','01:00:00');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `role` varchar(15) NOT NULL,
  `accessPrivilege` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`role`,`accessPrivilege`) values ('admin','system administrator'),('manager','station manager'),('presenter','radio program presenter'),('producer','program producer');



/*Table structure for table `weekly-schedule` */

DROP TABLE IF EXISTS `weekly-schedule`;

CREATE TABLE `weekly-schedule` (
  `startDate` datetime NOT NULL,
  `assignedBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`startDate`),
  UNIQUE KEY `startDate_UNIQUE` (`startDate`),
  KEY `id_assigned_by` (`assignedBy`),
  CONSTRAINT `id_ws` FOREIGN KEY (`assignedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `weekly-schedule` */

insert  into `weekly-schedule`(`startDate`,`assignedBy`) values ('2016-01-03 00:00:00','pointyhead'),('2016-01-10 00:00:00','pointyhead'),('2016-01-17 00:00:00','pointyhead'),('2016-01-24 00:00:00','pointyhead'),('2016-01-31 00:00:00','pointyhead'),('2016-02-07 00:00:00','pointyhead'),('2016-02-14 00:00:00','pointyhead'),('2016-02-21 00:00:00','pointyhead'),('2016-02-28 00:00:00','pointyhead'),('2016-03-06 00:00:00','pointyhead'),('2016-03-13 00:00:00','pointyhead'),('2016-03-20 00:00:00','pointyhead'),('2016-03-27 00:00:00','pointyhead'),('2016-04-03 00:00:00','pointyhead'),('2016-04-10 00:00:00','pointyhead'),('2016-04-17 00:00:00','pointyhead'),('2016-04-24 00:00:00','pointyhead'),('2016-05-01 00:00:00','pointyhead'),('2016-05-08 00:00:00','pointyhead'),('2016-05-15 00:00:00','pointyhead'),('2016-05-22 00:00:00','pointyhead'),('2016-05-29 00:00:00','pointyhead'),('2016-06-05 00:00:00','pointyhead'),('2016-06-12 00:00:00','pointyhead'),('2016-06-19 00:00:00','pointyhead'),('2016-06-26 00:00:00','pointyhead'),('2016-07-03 00:00:00','pointyhead'),('2016-07-10 00:00:00','pointyhead'),('2016-07-17 00:00:00','pointyhead'),('2016-07-24 00:00:00','pointyhead'),('2016-07-31 00:00:00','pointyhead'),('2016-08-07 00:00:00','pointyhead'),('2016-08-14 00:00:00','pointyhead'),('2016-08-21 00:00:00','pointyhead'),('2016-08-28 00:00:00','pointyhead'),('2016-09-04 00:00:00','pointyhead'),('2016-09-11 00:00:00','pointyhead'),('2016-09-18 00:00:00','pointyhead'),('2016-09-25 00:00:00','pointyhead'),('2016-10-02 00:00:00','pointyhead'),('2016-10-09 00:00:00','pointyhead'),('2016-10-16 00:00:00','pointyhead'),('2016-10-23 00:00:00','pointyhead'),('2016-10-30 00:00:00','pointyhead'),('2016-11-06 00:00:00','pointyhead'),('2016-11-13 00:00:00','pointyhead'),('2016-11-20 00:00:00','pointyhead'),('2016-11-27 00:00:00','pointyhead'),('2016-12-04 00:00:00','pointyhead'),('2016-12-11 00:00:00','pointyhead'),('2016-12-18 00:00:00','pointyhead'),('2016-12-25 00:00:00','pointyhead'),('2017-01-01 00:00:00','pointyhead'),('2017-01-08 00:00:00','pointyhead'),('2017-01-15 00:00:00','pointyhead'),('2017-01-22 00:00:00','pointyhead'),('2017-01-29 00:00:00','pointyhead'),('2017-02-05 00:00:00','pointyhead'),('2017-02-12 00:00:00','pointyhead'),('2017-02-19 00:00:00','pointyhead'),('2017-02-26 00:00:00','pointyhead'),('2017-03-05 00:00:00','pointyhead'),('2017-03-12 00:00:00','pointyhead'),('2017-03-19 00:00:00','pointyhead'),('2017-03-26 00:00:00','pointyhead'),('2017-04-02 00:00:00','pointyhead'),('2017-04-09 00:00:00','pointyhead'),('2017-04-16 00:00:00','pointyhead'),('2017-04-23 00:00:00','pointyhead'),('2017-04-30 00:00:00','pointyhead'),('2017-05-07 00:00:00','pointyhead'),('2017-05-14 00:00:00','pointyhead'),('2017-05-21 00:00:00','pointyhead'),('2017-05-28 00:00:00','pointyhead'),('2017-06-04 00:00:00','pointyhead'),('2017-06-11 00:00:00','pointyhead'),('2017-06-18 00:00:00','pointyhead'),('2017-06-25 00:00:00','pointyhead'),('2017-07-02 00:00:00','pointyhead'),('2017-07-09 00:00:00','pointyhead'),('2017-07-16 00:00:00','pointyhead'),('2017-07-23 00:00:00','pointyhead'),('2017-07-30 00:00:00','pointyhead'),('2017-08-06 00:00:00','pointyhead'),('2017-08-13 00:00:00','pointyhead'),('2017-08-20 00:00:00','pointyhead'),('2017-08-27 00:00:00','pointyhead'),('2017-09-03 00:00:00','pointyhead'),('2017-09-10 00:00:00','pointyhead'),('2017-09-17 00:00:00','pointyhead'),('2017-09-24 00:00:00','pointyhead'),('2017-10-01 00:00:00','pointyhead'),('2017-10-08 00:00:00','pointyhead'),('2017-10-15 00:00:00','pointyhead'),('2017-10-22 00:00:00','pointyhead'),('2017-10-29 00:00:00','pointyhead'),('2017-11-05 00:00:00','pointyhead'),('2017-11-12 00:00:00','pointyhead'),('2017-11-19 00:00:00','pointyhead'),('2017-11-26 00:00:00','pointyhead'),('2017-12-03 00:00:00','pointyhead'),('2017-12-10 00:00:00','pointyhead'),('2017-12-17 00:00:00','pointyhead'),('2017-12-24 00:00:00','pointyhead'),('2017-12-31 00:00:00','pointyhead');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
