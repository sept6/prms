/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.impl.ScheduleDAOImpl;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 *
 * @author andy
 */
public class ScheduleServiceTest extends TestCase {
     
    private ScheduleDAO daoMock = null;
    
    public ScheduleServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       daoMock = mock(ScheduleDAOImpl.class);   
    }
    
    @After
    public void tearDown() {
        daoMock = null;
    }

    /**
     * Test of processCopy method, of class ScheduleService.
     */
    @Test
    public void testProcessCopy() throws SQLException, NotFoundException {
        System.out.println("processCopy");
        List<ProgramSlot> programSlot = new ArrayList<ProgramSlot>();
        ProgramSlot p1 = new ProgramSlot();
        ProgramSlot p2 = new ProgramSlot();
        ScheduleService instance = new ScheduleService();
        instance.setScheduleDAO(daoMock);
        reset(daoMock);
        
        //Test if duplicate program slot insert
         p1.setAssignedBy("Admin");
        p1.setProducer(new User("produceruser"));
        p1.setPresenter(new User("presenteruser"));
        p1.setRadioProgram(new RadioProgram("Classic Music"));
        //String programDate = "30/09/2016 00:00:00";
        //Date programStartDate = Utility.toDate(programDate,Utility.DATE_TIME_PATTERN);
        p1.setDateOfProgram(new Date(System.currentTimeMillis() + (2*60*60*1000)));
        p1.setStartTime(new Date(System.currentTimeMillis() + (2*60*60*1000)));
        p1.setDuration(Time.valueOf("01:00:00"));
        
        try{
             when(daoMock.create(p1)).thenReturn(true);
             programSlot.add(p1);
            instance.processCopy(programSlot);
        }catch(Exception ex){
            fail();
        }
       
        
        //Test if distinct program slot insert
        p1.setAssignedBy("Admin");
        p1.setProducer(new User("produceruser"));
        p1.setPresenter(new User("presenteruser"));
        p1.setRadioProgram(new RadioProgram("Classic Music"));
        p1.setDateOfProgram(new Date());
        p1.setStartTime(new Date(System.currentTimeMillis() - (2*60*60*1000)));
        p1.setDuration(Time.valueOf("01:00:00"));
        
        p2.setAssignedBy("Admin");
        p2.setProducer(new User("produceruser"));
        p2.setPresenter(new User("presenteruser"));
        p2.setRadioProgram(new RadioProgram("Classic Music"));
        p2.setDateOfProgram(new Date());
        p2.setStartTime(new Date(System.currentTimeMillis() - (2*60*60*1000)));
        p2.setDuration(Time.valueOf("02:00:00"));
        
        programSlot.clear();
        programSlot.add(p1);
        programSlot.add(p2);
        
        try{
            when(daoMock.create(p1)).thenReturn(true);
            when(daoMock.create(p2)).thenReturn(true);
            instance.processCopy(programSlot);
        }catch(Exception ex){
            fail();
        }
       
    }

    /**
     * Test of processCreateAnnualWeeklySchedule method, of class ScheduleService.
     */
    @Test
    public void testProcessCreateAnnualWeeklySchedule() throws SQLException {
        System.out.println("processCreateAnnualWeeklySchedule");
        
        ScheduleService instance = new ScheduleService();
        instance.setScheduleDAO(daoMock);
        
        // Test normal creation
        reset(daoMock);
        AnnualSchedule annualSch = new AnnualSchedule(Calendar.getInstance().get(Calendar.YEAR));
        when(daoMock.isAnnualScheduleExist(Calendar.getInstance().get(Calendar.YEAR))).thenReturn(false);
        String result = instance.processCreateAnnualWeeklySchedule(annualSch);
        assertEquals("", result);
        verify(daoMock).createAnnualWeeklySchedule(annualSch);
        
        // Test past schedule creation
        reset(daoMock);
        AnnualSchedule annualSch2 = new AnnualSchedule(Calendar.getInstance().get(Calendar.YEAR) - 1);
        result = instance.processCreateAnnualWeeklySchedule(annualSch2);
        assertEquals("Cannot create past annual schedule", result);
        verify(daoMock, never()).createAnnualWeeklySchedule(annualSch2);

        // Test duplicate schedule
        reset(daoMock);
        AnnualSchedule annualSch3 = new AnnualSchedule(Calendar.getInstance().get(Calendar.YEAR) + 1);
        when(daoMock.isAnnualScheduleExist(Calendar.getInstance().get(Calendar.YEAR) + 1)).thenReturn(true);
        result = instance.processCreateAnnualWeeklySchedule(annualSch3);
        assertEquals("Annual Schedule already exist", result);
        verify(daoMock, never()).createAnnualWeeklySchedule(annualSch3);
    }

    /**
     * Test of generateWeeklySchedule method, of class ScheduleService.
     */
    @Test
    public void testGenerateWeeklySchedule() {
        System.out.println("generateWeeklySchedule");
        AnnualSchedule annualSch = new AnnualSchedule(2016);
        ScheduleService instance = new ScheduleService();
        ArrayList<WeeklySchedule> result = instance.generateWeeklySchedule(annualSch);
        for(WeeklySchedule r:result) {
            Calendar c = Calendar.getInstance();
            c.setTime(r.getStartDate());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            assertEquals(1, dayOfWeek);
            
            assertEquals(2016, c.get(Calendar.YEAR));
        }
    }

    /**
     * Test of processCreate method, of class ScheduleService.
     */
    @Test
    public void testProcessCreate() throws NotFoundException, SQLException {
        System.out.println("processCreate");
        ProgramSlot programSlot = null;
        ScheduleService instance = new ScheduleService();
        instance.setScheduleDAO(daoMock);
        reset(daoMock);

        try {
            when(daoMock.create(programSlot)).thenThrow(NullPointerException.class);
            instance.processCreate(programSlot); 
            fail();
        } catch (NullPointerException e){
        } 
        programSlot = new ProgramSlot();
        programSlot.setAssignedBy("Admin");
        programSlot.setProducer(new User("produceruser"));
        programSlot.setPresenter(new User("presenteruser"));
        programSlot.setRadioProgram(new RadioProgram("Classic Music"));
        programSlot.setDateOfProgram(new Date());
        programSlot.setStartTime(new Date(System.currentTimeMillis() - (2*60*60*1000)));
        programSlot.setDuration(Time.valueOf("01:00:00"));

        try {    
            when(daoMock.create(programSlot)).thenReturn(true);
            instance.processCreate(programSlot);
        } catch (Exception ex) {
            fail();
        } 
        
        try {    
            programSlot.setDuration(null);
            when(daoMock.create(programSlot)).thenThrow(RuntimeException.class);
            instance.processCreate(programSlot);
            fail();
        } catch (RuntimeException ex) {
        } 
    }

    /**
     * Test of processModify method, of class ScheduleService.
     */
    @Test
    public void testProcessModify() {
        System.out.println("processModify");
        ScheduleService instance = new ScheduleService();
        instance.setScheduleDAO(daoMock);
        reset(daoMock);
        
        ProgramSlot programSlot = new ProgramSlot();
        programSlot.setAssignedBy("Admin");
        programSlot.setProducer(new User("produceruser"));
        programSlot.setPresenter(new User("presenteruser"));
        programSlot.setRadioProgram(new RadioProgram("Classic Music"));
        programSlot.setDateOfProgram(new Date(System.currentTimeMillis() + (2*60*60*1000)));
        programSlot.setStartTime(new Date(System.currentTimeMillis() + (2*60*60*1000)));
        programSlot.setDuration(Time.valueOf("00:30:00"));
        
        ProgramSlot newProgramSlot = new ProgramSlot();
        newProgramSlot.setAssignedBy(programSlot.getAssignedBy());
        newProgramSlot.setProducer(programSlot.getProducer());
        newProgramSlot.setPresenter(programSlot.getPresenter());
        newProgramSlot.setRadioProgram(programSlot.getRadioProgram());
        newProgramSlot.setDateOfProgram(programSlot.getDateOfProgram());
        newProgramSlot.setStartTime(programSlot.getStartTime());
        newProgramSlot.setDuration(programSlot.getDuration());
             
        try{
             when(daoMock.delete(programSlot)).thenReturn(true);
             when(daoMock.create(programSlot)).thenReturn(true);
             
             newProgramSlot.setAssignedBy("Andy");
             instance.processModify(newProgramSlot, programSlot);
             
             verify(daoMock).delete(programSlot);
             verify(daoMock).create(newProgramSlot);
             
        }catch(Exception ex){
            fail("Exception occurs in ProcessModify");
        }
        //instance.processModify(newSlot, programToBeDeleted);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of processDelete method, of class ScheduleService.
     */
    @Test
    public void testProcessDelete() throws NotFoundException, SQLException {
        System.out.println("processDelete");
        ScheduleService instance = new ScheduleService();
        instance.setScheduleDAO(daoMock);
        
        // Test normal creation
        reset(daoMock);
        
        // Test past schedule delete.
        ProgramSlot slot = new ProgramSlot();
        slot.setAssignedBy("Admin");
        slot.setProducer(new User("producer"));
        slot.setPresenter(new User("presenter"));
        slot.setRadioProgram(new RadioProgram("Classic Music"));
        slot.setDateOfProgram(new Date());
        slot.setStartTime(new Date(System.currentTimeMillis() - (2*60*60*1000)));
        slot.setDuration(Time.valueOf("01:00:00"));
        when(daoMock.create(slot)).thenReturn(true);
        
        String result = instance.processDelete(slot);
        assertEquals("error.deletePresentingPresentedError", result);
        verify(daoMock, never()).delete(slot);
        
        // Test presenting schedule delete.
        //String programDate = "30/09/2016 00:00:00";
        //Date programStartDate = Utility.toDate(programDate,Utility.DATE_TIME_PATTERN);
        slot.setStartTime(new Date(System.currentTimeMillis() - (2*60*60*1000)));
        when(daoMock.create(slot)).thenReturn(true);
        
        result = instance.processDelete(slot);
        assertEquals("error.deletePresentingPresentedError", result);
        verify(daoMock, never()).delete(slot);
        
        // Test pending/oncoming schedule delete.
        slot.setStartTime(new Date(System.currentTimeMillis() + (2*60*60*1000)));
        when(daoMock.create(slot)).thenReturn(true);
        
        result = instance.processDelete(slot);
        assertEquals(null, result);
        verify(daoMock).delete(slot);
        
        reset(daoMock);
    }

    /**
     * Test of isTimeSlotAvailable method, of class ScheduleService.
     */
    @Test
    public void testIsTimeSlotAvailable() {
        System.out.println("isTimeSlotAvailable");
        ScheduleService instance = new ScheduleService();
        boolean expResult = true;
        
        String programDate = "28/09/2016 23:30:00";
        Date programStartDate = Utility.toDate(programDate,Utility.DATE_TIME_PATTERN);
        
        try{
            when(daoMock.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()))).thenReturn(false);
            boolean result = instance.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()));
            assertNotEquals(expResult, result);
            
            programDate = "30/10/2016 00:00:00";
             programStartDate = Utility.toDate(programDate,Utility.DATE_TIME_PATTERN);
             when(daoMock.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()))).thenReturn(true);
            result = instance.isTimeSlotAvailable(new Timestamp(programStartDate.getTime()));
            assertEquals(expResult, result);
            
        }catch(Exception ex){
            fail();
        }
         
     
       
        
        
    }
    
    /**
     * Test of displayPresenter and displayProducer method, of class ReviewSelectPresenterProducerService.
     */
    @Test
    public void testReviewSelectPresenterProducer(){
        System.out.println("ReviewSelectPresenterProducer");
        ReviewSelectPresenterProducerService instance = new ReviewSelectPresenterProducerService();
        List<User> presenterList = null;
        List<User> producerList = null;
        presenterList = instance.displayPresenter();
        producerList = instance.displayProducer();
        //List<User> producerList = instance.displayProducer();
        for(int i = 0; i < presenterList.size(); i++){
            System.out.println("presenter-" + i +": ID: "+presenterList.get(i).getId());
        }
        for(int i = 0; i < producerList.size(); i++){
            System.out.println("producer-" + i +": ID: "+producerList.get(i).getId());
        }
    }
}
