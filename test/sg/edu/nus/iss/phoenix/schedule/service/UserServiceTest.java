/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.service;

import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDaoImpl;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.schedule.dao.impl.ScheduleDAOImpl;
import sg.edu.nus.iss.phoenix.schedule.entity.AnnualSchedule;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.schedule.entity.WeeklySchedule;
import sg.edu.nus.iss.phoenix.user.service.MaintainUserService;
import sg.edu.nus.iss.phoenix.util.Utility;

/**
 *
 * @author Kyaw Min Thu
 */
public class UserServiceTest {
    
    @Mock
    private UserDao uDaoMock;
    @Mock
    private ScheduleDAO sDaoMock;
    private MaintainUserService service;
    
    public UserServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new MaintainUserService();
        service.setSdao(sDaoMock);
        service.setUdao(uDaoMock);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processCreate method, of class MaintainUserService.
     */
    @Test
    public void testProcessCreate() {        
        User user = new User();
        user.setAll("user1", "password", "User 1", "manager");
        try {
            service.processCreate(user);
            verify(uDaoMock).create(user);
        } catch(Exception ex) {
            fail();
        }
        reset(uDaoMock);
    }

    /**
     * Test of processModify method, of class MaintainUserService.
     */
    @Test
    public void testProcessModify() {
        try {
            // Create User.
            User user = new User("user2", "password", "User 1", "presenter");
            
            when(uDaoMock.getObject("user2")).thenReturn(user);
            
            User user2 = new User();
            user2.setAll("user2", "password2", "User 2", "producer");
            String result = service.processModify(user2);
            assertEquals(null, result);
            verify(uDaoMock).save(user2);
            
            when(uDaoMock.getObject("user2")).thenReturn(user);
            ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass( User.class );
            verify( uDaoMock ).save( userCaptor.capture() );
            User updatedUser = userCaptor.getValue();
            assertEquals( "User 2", updatedUser.getName());
            assertEquals( "password2", updatedUser.getPassword());
            assertEquals( "producer", updatedUser.getRoles().get(0).getRole());
        } catch(Exception ex) {
            fail();
        }
        reset(uDaoMock);
    }
    
    /**
     * Test of processDelete method, of class MaintainUserService.
     */
    @Test
    public void testProcessDelete() {
        try {
            // Create User.
            User user = new User("user1", "password", "User 1", "presenter");
            
            when(uDaoMock.getObject("user1")).thenReturn(user);
            String result = service.processDelete("user3");
            assertEquals(null, result);
        } catch(Exception ex) {
            fail();
        }
        reset(uDaoMock);
    }
    /**
     * Test of isUserExists method, of class MaintainUserService.
     */
    @Test
    public void testIsUserExists() {
        MaintainUserService actualService = new MaintainUserService();
        try {
//            User user = new User("user1", "password", "User 1", "presenter");
//            when(uDaoMock.create(user)).thenReturn(true);
            boolean result = actualService.isUserExists("catbert");
            assertTrue(result);
        } catch(Exception ex) {
            fail();
        }
        reset(uDaoMock);
    }
    /**
     * Test of getUserList method, of class MaintainUserService.
     */
    @Test
    public void testGetUserList() {
        MaintainUserService actualService = new MaintainUserService();
        try {
            List<User> result = actualService.getUserList();
            assertNotEquals(0, result.size());
        } catch(Exception ex) {
            fail();
        }
        reset(uDaoMock);
    }
}
