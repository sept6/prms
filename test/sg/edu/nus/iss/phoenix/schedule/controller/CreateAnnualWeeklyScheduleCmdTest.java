package sg.edu.nus.iss.phoenix.schedule.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Kevin
 */
public class CreateAnnualWeeklyScheduleCmdTest extends TestCase {
    
    private CreateAnnualWeeklyScheduleCmd cmd;
    private HttpServletRequest reqMock;
    private HttpServletResponse resMock;
    
    @Before
    public void setUp() {
        cmd = new CreateAnnualWeeklyScheduleCmd();
        reqMock = mock(HttpServletRequest.class);
        resMock = mock(HttpServletResponse.class);
    }
    
    @After
    public void tearDown() {
        cmd = null;
        reqMock = null;
        resMock = null;
    }
    
    @Test
    public void testPerform() throws IOException, ServletException {
        // Test empty date
        reset(reqMock);
        reset(resMock);
        when(reqMock.getParameter("annualschyear")).thenReturn("");
        String result = cmd.perform("", reqMock, resMock);   
        verify(reqMock, atLeast(1)).getParameter("annualschyear");
        assertEquals("/pages/setupannualweeklysch.jsp", result);
        verify(reqMock).setAttribute("error", "Invalid year");;
        
        // Test wrong date format
        reset(reqMock);
        reset(resMock);
        when(reqMock.getParameter("annualschyear")).thenReturn("aaaa");
        result = cmd.perform("", reqMock, resMock);
        verify(reqMock, atLeast(1)).getParameter("annualschyear");
        verify(reqMock).setAttribute("error", "Invalid year");
        assertEquals("/pages/setupannualweeklysch.jsp", result);
    }    
}
