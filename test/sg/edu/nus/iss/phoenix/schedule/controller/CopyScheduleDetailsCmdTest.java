/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.schedule.entity.ProgramSlot;

/**
 *
 * @author andy
 */
public class CopyScheduleDetailsCmdTest extends TestCase{
    
    private CopyScheduleDetailsCmd cmd;
    private HttpServletRequest reqMock;
    private HttpServletResponse resMock;
    private HttpSession sessMock;
    
    public CopyScheduleDetailsCmdTest() {
    }
      
    @Before
    public void setUp() {
        cmd = new CopyScheduleDetailsCmd();
        reqMock = mock(HttpServletRequest.class);
        resMock = mock(HttpServletResponse.class);
        sessMock = mock(HttpSession.class);
    }
    
    @After
    public void tearDown() {
        cmd = null;
        reqMock = null;
        resMock = null;
    }

    /**
     * Test of perform method, of class CopyScheduleDetailsCmd.
     */
    @Test
    public void testPerform() throws Exception {
         //Test no program slot to copy over
        String path = "";
        reset(reqMock);
        reset(resMock);
        when(reqMock.getSession()).thenReturn(sessMock);
        when(sessMock.getAttribute("selectedProgramSlot")).thenReturn(null);
       
        String result = cmd.perform(path, reqMock, resMock);
        assertEquals("/pages/crudsch.jsp", result);
        verify(reqMock).setAttribute("error", 
                "Please select at least one program slot to copy.");
        
        //test timeslot is not available
        
        
        //Test program slot to copy(meet all conditions)
        ProgramSlot programSlot = new ProgramSlot();
        programSlot.setAssignedBy("Admin");
        programSlot.setProducer(new User("produceruser"));
        programSlot.setPresenter(new User("presenteruser"));
        programSlot.setRadioProgram(new RadioProgram("Classic Music"));
        
        programSlot.setDateOfProgram(new Date(System.currentTimeMillis() + (10*60*60*1000)));
        programSlot.setStartTime(new Date(System.currentTimeMillis() + (10*60*60*1000)));
        programSlot.setDuration(Time.valueOf("00:30:00"));
        List<ProgramSlot> selectedProgramSlot = new ArrayList<ProgramSlot>();
        selectedProgramSlot.add(programSlot);
        
        result = cmd.perform(path, reqMock, resMock);
        verify(reqMock.getSession(), atLeast(1)).getAttribute("selectedProgramSlot");
       
        assertEquals("/pages/crudsch.jsp", result);
        //when(reqMock.setAttribute("selectedProgramSlot", selectedProgramSlot));
        
        
        
    }
    
}
