package sg.edu.nus.iss.phoenix.schedule.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Kevin
 */
public class SearchRadioProgramScheduleCmdTest extends TestCase {
    
    private SearchRadioProgramScheduleCmd cmd;
    private HttpServletRequest reqMock;
    private HttpServletResponse resMock;
    
    @Before
    public void setUp() {
        cmd = new SearchRadioProgramScheduleCmd();
        reqMock = mock(HttpServletRequest.class);
        resMock = mock(HttpServletResponse.class);
    }
    
    @After
    public void tearDown() {
        cmd = null;
        reqMock = null;
        resMock = null;
    }
    
    @Test
    public void testPerform() throws IOException, ServletException {
        // Test empty date
        reset(reqMock);
        reset(resMock);
        when(reqMock.getParameter("startDate")).thenReturn("");
        String result = cmd.perform("", reqMock, resMock);   
        verify(reqMock, atLeast(1)).getParameter("startDate");
        assertEquals("/pages/crudsch.jsp", result);
        verify(reqMock).setAttribute("error", 
                "Please specify start date in search criteria");
        
        // Test wrong date format
        reset(reqMock);
        reset(resMock);
        when(reqMock.getParameter("startDate")).thenReturn("2016-10-01");
        result = cmd.perform("", reqMock, resMock);
        verify(reqMock, atLeast(1)).getParameter("startDate");
        verify(reqMock).setAttribute("error", "Invalid date format");
        assertEquals("/pages/crudsch.jsp", result);
    }
}
