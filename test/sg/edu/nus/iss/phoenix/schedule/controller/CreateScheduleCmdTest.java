/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.schedule.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author Raghavendra S
 */
public class CreateScheduleCmdTest extends TestCase{
    
    private CreateScheduleCmd createCmd;
    private HttpServletRequest reqMock;
    private HttpServletResponse resMock;
    private HttpSession sessionMock;
    private List<String> errorList;

    @Before
    public void setUp() {
        createCmd = new CreateScheduleCmd();
        errorList = new ArrayList<String>();
        reqMock = mock(HttpServletRequest.class);
        resMock = mock(HttpServletResponse.class);
        sessionMock = mock(HttpSession.class);
    }
    
    @After
    public void tearDown() {
        createCmd = null;
        reqMock = null;
        resMock = null;
        sessionMock = null;
    }
    
    @Test
    public void testperform(){
        
        reset(reqMock);
        reset(resMock);      
        reset(sessionMock);      
        errorList.add("error.invalidprogramname");
        errorList.add("error.invalidpresenterid");
        errorList.add("error.invalidproducerid");
        errorList.add("error.invalidstartdatetime");
        errorList.add("error.invalidduration");
        
        User user = new User();
        user.setAll("testuserid", "test", "Test user", "admin");                
        when(sessionMock.getAttribute("user")).thenReturn(user);
                
        when(reqMock.getParameter("programName")).thenReturn(null);
        when(reqMock.getParameter("presenterId")).thenReturn(null);
        when(reqMock.getParameter("producerId")).thenReturn(null);
        when(reqMock.getParameter("dateOfProgram")).thenReturn(null);
        when(reqMock.getParameter("starttime")).thenReturn(null);

        when(reqMock.getSession()).thenReturn(sessionMock);
        
        try {
            String returnPath = createCmd.perform("/pages/success.jsp", reqMock, resMock);
            assertEquals(returnPath, "/pages/customerror.jsp");
            
            verify(reqMock).setAttribute("message", "error.createschedulefailed");
            verify(reqMock).setAttribute("errorList", errorList);
            
        } catch (IOException | ServletException ex) {
            Logger.getLogger(CreateScheduleCmdTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
