package sg.edu.nus.iss.phoenix.testsuite;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import sg.edu.nus.iss.phoenix.schedule.controller.CopyScheduleDetailsCmdTest;
import sg.edu.nus.iss.phoenix.schedule.controller.CreateAnnualWeeklyScheduleCmdTest;
import sg.edu.nus.iss.phoenix.schedule.controller.SearchRadioProgramScheduleCmdTest;
import sg.edu.nus.iss.phoenix.schedule.service.ScheduleServiceTest;

/**
 *
 * @author Kevin
 */
public class AllTest extends TestCase {
    
    public static TestSuite suite() {
        TestSuite suite = new TestSuite("All Test");
        
        suite.addTestSuite(CreateAnnualWeeklyScheduleCmdTest.class);
        suite.addTestSuite(SearchRadioProgramScheduleCmdTest.class);
        
        suite.addTestSuite(ScheduleServiceTest.class);
        suite.addTestSuite(CopyScheduleDetailsCmdTest.class);
        return suite;
    }
    
}
